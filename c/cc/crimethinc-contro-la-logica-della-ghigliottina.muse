#title Contro la logica della ghigliottina
#subtitle Perché la Comune di Parigi ha bruciato la ghigliottina — e perché anche noi dovremmo farlo
#author CrimethInc
#LISTtitle Contro la logica della ghigliottina
#SORTauthors CrimethInc
#SORTtopics analisi, storia
#date 8 aprile 2019
#source Consultato il 17/8/2022 su [[https://it.crimethinc.com/2019/04/08/contro-la-logica-della-ghigliottina-perche-la-comune-di-parigi-ha-bruciato-la-ghigliottina-e-perche-anche-noi-dovremmo-farlo][it.crimethinc.com]]
#lang it
#pubdate 2022-09-17T22:00:00



148 anni fa oggi, il 6 aprile 1871, i partecipanti armati alla rivoluzionaria Comune di Parigi sequestrarono la ghigliottina che era stata riposta vicino alla prigione di Parigi. Dopo averla portata ai piedi della statua di Voltaire, la fecero a pezzi per poi bruciarla in un falò, accompagnati dall’applauso di una folla immensa.[1] Quest’azione popolare nata dal basso, non fu uno spettacolo organizzato dai politici. In quel periodo, la Comune controllava Parigi, dove vivevano ancora tutte persone appartenenti a tutte le estrazioni sociali; l’Esercito francese e quello prussiano, dopo averla circondata, si stavano preparando a invaderla per imporre il Governo repubblicano conservatore di Adolphe Thiers. In quelle circostanze, bruciare la ghigliottina fu un gesto coraggioso per ripudiare sia il Regno del Terrore sia l’idea che un cambiamento sociale positivo potesse essere ottenuto massacrando la gente.

<em>“Cosa?”</em> potresti esclamare sconvolto, “i comunardi <em>bruciarono</em> la ghigliottina? Perché diavolo avrebbero dovuto <em>farlo?</em> Pensavo che la ghigliottina fosse un simbolo di liberazione!”

Perché, infatti? Se la ghigliottina non è un simbolo di liberazione, allora per quale motivo negli ultimi anni è diventata un tale leitmotiv per la sinistra radicale? Perché il Web è pieno di meme con la ghigliottina? Perché The Coup cantano “We got the guillotine, you better run?” (“Abbiamo la ghigliottina, è meglio se corri”). Il più famoso periodico socialista si chiama <em>Jacobin,</em> in onore dei primi sostenitori della ghigliottina. Di sicuro, tutto questo non può essere solo un’ironica parodia dell’ansia persistente che la destra nutre dei confronti della Rivoluzione francese.

La ghigliottina è giunta a occupare il nostro immaginario collettivo. In un periodo in cui le spaccature all’interno della nostra società stanno espandendosi verso la guerra civile, la ghigliottina è l’incarnazione di una vendetta sanguinaria senza compromessi. Rappresenta l’idea che la violenza dello Stato potrebbe essere una buona cosa solo se <em>la gente giusta</em> fosse al potere.

Coloro che danno per scontata la propria impotenza presumono di poter promuovere orribili fantasie di vendetta senza conseguenze alcune. Ma se siamo davvero intenzionati a cambiare il mondo, è nostro dovere assicurarci che le nostre proposte non siano ugualmente raccapriccianti.

*** Vendetta

Non sorprende che, oggi, la gente voglia vendette sanguinarie. Il profitto capitalistico sta rapidamente rendendo il pianeta inabitabile. I funzionari dell’US Border Patrol stanno rapendo, drogando e imprigionando i bambini. Singoli atti di violenza razzista e misogina occorrono costantemente. Per molte persone, la vita quotidiana è umiliante e lascia sempre più privi di forze.

Coloro che non desiderano vendicarsi perché non sono dotati di abbastanza pìetas per indignarsi di fronte all’ingiustizia o perché <em>semplicemente non vi prestano attenzione,</em> non meritano nulla. C’è meno virtù nell’indifferenza che nei peggiori eccessi di vendetta.

Voglio vendicarmi degli agenti di polizia che ammazzano le persone impunemente, dei miliardari che si arricchiscono attraverso sfruttamento e gentrification? Degli estremisti che vessano e condannano il prossimo? Hanno ucciso persone che conoscevo; stanno cercando di distruggere tutto ciò che amo. Quando penso al male che stanno causando, mi sento pronto a spezzar loro ossa, a ucciderli a mani nude.

Ma quel desiderio è lontano dalla mia politica. Posso desiderare qualcosa senza dover decodificare una giustificazione politica per questo. Posso desiderare qualcosa e scegliere di non esaudire il mio desiderio, se c’è qualcos’altro che voglio ancor di più – in questo caso, una rivoluzione anarchica non basata sulla vendetta. Non giudico chi la vuole, soprattutto se ha vissuto cose peggiori rispetto a quelle che ho vissuto io, ma non confondo quel desiderio con una proposta di liberazione.

Se il tipo di sete di sangue che descrivo ti spaventa, o se ti sembra semplicemente sconveniente, allora non dovresti assolutamente scherzare su <em>altre</em> persone che commettono omicidi industrializzati per conto tuo.

Poiché questo è ciò che contraddistingue la fantasia della ghigliottina: si tratta di efficienza e distanza. Quelli che feticizzano la ghigliottina non vogliono uccidere le persone a mani nude; non sono pronti a lacerare le carni di qualcuno con i propri denti. Vogliono che la loro vendetta sia automatizzata ed effettuata per loro. Sono come quelli che mangiano spensierati delle Chicken McNuggets ma non potrebbero mai macellare personalmente una mucca o abbattere una foresta pluviale. Preferiscono che lo spargimento di sangue avvenga in modo ordinato, con tutti gli incartamenti compilati correttamente, seguendo il modello di giacobini e bolscevichi a imitazione del funzionamento impersonale dello Stato capitalista.

E un’altra cosa: non vogliono assumersene la responsabilità. Preferiscono esprimere le proprie fantasie in modo paradossale, negando l’evidenza. Tuttavia, chiunque sia mai stato parte attiva in dei cambiamenti sociali sa quanto può essere sottile la linea che separa fantasia e realtà. Diamo un’occhiata al ruolo “rivoluzionario” ricoperto in passato dalla ghigliottina.

<quote>

“<em>Ma la vendetta è indegna di un anarchico!</em> <em>Il domani, il nostro domani, non vuole liti, né crimini, né bugie; afferma invece vita, amore<strong>, conoscenza; lavoriamo per giungere a questo giorno</strong>.</em>”

— Kurt Gustav Wilckens—anarchico, pacifista che assassinò il colonnello Héctor Varela, il funzionario argentino che aveva supervisionato il massacro di circa 1.500 lavoratori in sciopero in Patagonia.

</quote>

*** Brevissima storia della ghigliottiona

La ghigliottina è associata alla politica radicale perché, il 21 gennaio 1793, durante la prima Rivoluzione francese, fu usata per decapitare, diversi mesi dopo il suo arresto, re Luigi XVI. Una volta aperto il vaso di Pandora della forza distruttrice, è difficile richiuderlo.

Dopo aver iniziato a sfruttare la ghigliottina come strumento di cambiamento sociale, Maximilien de Robespierre, presidente del Club dei giacobini, continuò a impiegarla per consolidare il potere della sua fazione del Governo Repubblicano. Com’è consuetudine dei demagoghi, Robespierre, Georges Danton e altri estremisti si avvalsero dell’assistenza dei <em>sanculotti</em> – i poveri arrabbiati – per spodestare, nel giugno 1793, la fazione più moderata dei girondini (anche questi erano giacobini; se ami un giacobino, la cosa migliore che puoi fare per lui sarà impedire che il suo partito salga al potere, dato che, di sicuro, sarà il prossimo con le spalle al muro dopo di te). Dopo aver ghigliottinato in massa i girondini, Robespierre si accinse a consolidare il potere a spese di Danton, il <em>sanculotto,</em> e di tutti gli altri.

<quote>

“Il Governo rivoluzionario non ha niente in comune con l’anarchia. Al contrario, il suo scopo è di reprimerla per instaurare e rafforzare il regno della Legge.”

— Maximilien Robespierre, fa una distinzione tra il suo Governo autocratico e i movimenti di base più radicali che contribuirono a far nascere la Rivoluzione francese.[2]

</quote>

All’inizio del 1794, Robespierre e i suoi alleati avevano mandato alla ghigliottina un gran numero di persone radicali quanto loro, tra cui Anaxagoras Chaumette e i cosiddetti Enragés (Arrabbiati), Jacques Hébert e i cosiddetti Hébertisti, la proto-femminista e abolizionista Olympe de Gou (che aveva avuto il coraggio di suggerire al suo amico d’infanzia Robespierre che “l’amore è più forte e più duraturo della paura”) – e la moglie di Desmoulins, per buona misura, nonostante sua sorella fosse stata la fidanzata di Robespierre. Predisposero anche la ghigliottina per Georges Danton e per i suoi sostenitori, insieme a vari altri ex alleati. Per festeggiare questo spargimento di sangue, Robespierre organizzò il Culto dell’Essere Supremo, una cerimonia pubblica obbligatoria che inaugurava una religione di Stato inventata.[3]

Dopo questo, passò solo un mese e mezzo prima che lo stesso Robespierre fosse ghigliottinato, avendo sterminato troppi tra quelli che avrebbero potuto combattere al suo fianco per contrastare la controrivoluzione. Tutto ciò pose le basi per un periodo di reazione che culminò con l’ascesa al potere e l’autoincorazione come Imperatore di Napoleone Bonaparte. Secondo il calendario repubblicano francese (un’innovazione che, pur non avendo riscosso successo, fu brevemente reintrodotta durante la Comune di Parigi), l’esecuzione di Robespierre ebbe luogo durante il Termidoro e, per questo motivo, sarà per sempre associata all’insorgere della controrivoluzione.

<quote>

“Robespierre uccise la Rivoluzione in tre mosse: l’esecuzione di Hébert, l’esecuzione di Danton, il Culto dell’Essere Supremo… La vittoria di Robespierre, lungi dal salvarla, avrebbe significato solo una caduta più grave e irreparabile.”

— Louis-Auguste Blanqui, lui stesso a malapena oppositore della violenza autoritaria.

</quote>

È però un errore concentrarsi su Robespierre; questi non era un tiranno sovrumano, tutt’al più era uno zelante burocrate che ricopriva un ruolo conteso da parecchi rivoluzionari e che se non fosse stato occupato da lui lo sarebbe stato da qualcun altro. La questione era sistemica — la competizione per il potere dittatoriale centralizzato — non legata a crimini individuali.

La tragedia del 1793-1795 conferma che qualsiasi strumento utilizzerai per provocare una rivoluzione sarà sicuramente usato contro di te. Ma il problema non è solo lo strumento, è la logica che vi sta dietro. Anziché demonizzare Robespierre — o Lenin, Stalin o Pol Pot — dobbiamo esaminare <em>la logica della ghigliottina.</em>

In un certo senso, possiamo comprendere perché Robespierre e i suoi contemporanei abbiano finito per fare affidamento sull’omicidio di massa come strumento politico. Erano minacciati da invasioni militari straniere, cospirazioni interne e rivolte controrivoluzionarie; stavano prendendo decisioni in un ambiente estremamente stressante. Ma se è possibile capire come siano arrivati ad accogliere la ghigliottina, è impossibile sostenere che tutti gli omicidi fossero necessari per assicurar loro la posizione in cui si trovavano. Le loro stesse esecuzioni confutano tale argomentazione in modo abbastanza eloquente.

Allo stesso modo, è sbagliato pensare che la ghigliottina fosse impiegata soprattutto contro la classe dominante, anche al culmine del dominio giacobino. Essendo burocrati consumati, i giacobini tenevano registri dettagliati. Tra il giugno 1793 e la fine di luglio 1794, in Francia furono ufficialmente condannate a morte 16.594 persone, tra cui 2.639 a Parigi. Delle esecuzioni formali perpetrate nel periodo del Terrore, solo l’8% furono dispensate agli aristocratici e il 6% a membri del clero; il resto fu suddiviso tra la classe media e i poveri, con la stragrande maggioranza delle vittime provenienti dai ceti più bassi.

Ciò che avvenne durante la prima Rivoluzione francese non fu un colpo di fortuna. Mezzo secolo dopo, la Rivoluzione francese del 1848 seguì un percorso analogo. A febbraio, una rivolta guidata dai poveri arrabbiati conferì potere ai politici repubblicani; a giugno, quando la vita sotto il nuovo Governo si rivelò solo leggermente migliore di quella sotto la Monarchia, i parigini si ribellarono ancora una volta e i politici ordinarono all’esercito di massacrarli in nome della Rivoluzione. Questo pose le basi affinché il nipote di Napoleone Bonaparte vincesse le elezioni presidenziali di dicembre, promettendo di “ristabilire l’ordine.” Tre anni dopo, avendo mandato in esilio tutti i politici repubblicani, Napoleone III abolì la Repubblica e s’incoronò imperatore — suggerendo la famosa battuta di Marx secondo il quale la storia si ripete, “la prima volta come tragedia, la seconda come farsa.”

Allo stesso modo, Adolphe Thiers, messo al potere dalla Rivoluzione francese del 1870, massacrò senza pietà alcuna la Comune di Parigi, ma questo non fece altro che spianare la strada a politici ancora più reazionari che, nel 1873, lo costrinsero a dimettersi. In tutti e tre i casi, possiamo vedere come i rivoluzionari intenti a esercitare il potere statale debbano abbracciare <em>la logica della ghigliottina</em> per acquisirla e quindi, avendo brutalmente schiacciato altri rivoluzionari nella speranza di consolidare il controllo, siano inevitabilmente sconfitti da più forze conservatrici.

Nel XX secolo, Lenin descrisse Robespierre come un bolscevico <em>avant la lettre,</em> affermando che il Terrore fosse come l’antenato del progetto bolscevico. Non fu l’unico a fare quel paragone.

“Saremo il nostro Termidoro,” affermò l’apologeta bolscevico Victor Serge citando Lenin mentre si stava preparando a massacrare i ribelli di Kronstadt. In altre parole, avendo schiacciato gli anarchici e tutti quelli più a sinistra di loro, i bolscevichi sarebbero sopravvissuti alla reazione diventando <strong>essi</strong> <strong>stessi</strong> la controrivoluzione. Allo scopo di reclutare ex ufficiali zaristi tra le loro fila avevano già reintrodotto gerarchie fisse nell’Armata Rossa; parallelamente alla vittoria sugli insorti di Kronstadt, reintrodussero il libero mercato e il capitalismo, sebbene sotto il controllo dello Stato. Alla fine, Stalin assunse la posizione una volta occupata da Napoleone.

Pertanto, la ghigliottina non è uno strumento di liberazione. Ciò era già chiaro nel 1795, ben oltre un secolo prima che i bolscevichi iniziassero il loro terrore, quasi due secoli prima che i Khmer Rossi sterminassero circa un quarto dei cambogiani.

Perché, quindi, la ghigliottina è tornata di moda come simbolo di resistenza alla tirannia? Trovare una risposta a questa domanda ci dirà qualcosa sulla psicologia del nostro tempo.

*** Feticizzare la violenza dello Stato

È scandaloso che, ancora oggi, gli estremisti si associno ai giacobini, una tendenza reazionaria alla fine del 1793. Ma non è difficile elaborare una spiegazione. Allora, come ora, c’erano persone che volevano pensare a se stesse come radicali senza dover davvero rompere in modo definitivo con le istituzioni e le pratiche a loro familiari. Come disse Marx, “la tradizione di tutte le generazioni scomparse pesa come un incubo sul cervello dei viventi.”

Se — per usare la famosa definizione di Max Weber — un aspirante Governo si qualifica come rappresentante <em>dello Stato</em> ottenendo un monopolio sull’uso legittimo della forza fisica in un determinato territorio, allora uno dei modi più persuasivi con cui può dimostrare la sua sovranità è quello di esercitare impunemente una forza letale. Questo spiega i vari <strong>resoconti</strong> secondo i quali, durante la Rivoluzione francese, le decapitazioni pubbliche fossero ritenute occasioni di festa o persino religiose. Prima di allora, le decapitazioni rappresentavano l’affermazione della sacra autorità del monarca; la conferma del fatto che i rappresentanti della Repubblica detenessero la sovranità — nel nome del Popolo, ovviamente — ebbe quando questi presiedettero alle esecuzioni. “Luigi deve morire affinché la Patria possa vivere,” aveva proclamato Robespierre, cercando di santificare la nascita del nazionalismo borghese battezzandolo letteralmente nel sangue del precedente ordine sociale. Una volta inaugurata su tali basi, la Repubblica richiese continui sacrifici per affermare la propria autorità.

È qui che possiamo cogliere l’essenza dello Stato: <em>può uccidere, ma non può dare la vita.</em> In quanto concentrazione di legittimità politica e forza coercitiva, può nuocere, ma non può istituire il tipo di libertà positiva che gli individui sperimentano quando sono radicati in comunità che si sostengono a vicenda. Non può creare il tipo di solidarietà che dà origine all’armonia tra persone. Il modo in cui usiamo lo Stato per fare qualcosa agli altri, può essere utilizzato dagli altri per fare qualcosa a noi — come ebbe modo di sperimentare Robespierre —, ma nessuno può impiegare l’apparato coercitivo dello Stato per la causa della liberazione.

<strong>Per i radicali, feticizzare la ghigliottina è come feticizzare lo Stato: significa celebrare uno strumento di morte che sarà sempre usato soprattutto contro di noi.</strong>

Chi è stato privato di una relazione positiva con la propria rappresentanza, cerca spesso un surrogato con cui identificarsi, un leader la cui violenza possa sostenere la vendetta che desidera come conseguenza per la propria impotenza. Nell’era di Trump, siamo tutti ben consapevoli di come questo appaia tra gli esautorati sostenitori della politica di estrema destra. Ma anche a sinistra esistono persone che si sentono impotenti e arrabbiate, persone che desiderano vendetta, persone che vogliono vedere quello stesso Stato che li ha schiacciati rivoltarsi contro i loro nemici.

<quote>

Ricordare i “tankies” delle atrocità e dei tradimenti perpetrati dai socialisti di Stato a partire dal 1917 in poi corrisponde a chiamare Trump razzista e sessista. Rendere pubblico il fatto che Trump sia un predatore sessuale seriale non ha fatto altro che renderlo più popolare all’interno della comunità misogina che lo appoggia; allo stesso modo, la storia insanguinata del Socialismo autoritario non può che renderlo più attraente per coloro motivati soprattutto dal desiderio di identificarsi con qualcosa di potente.

— Anarchici nell’era di Trump

</quote>

Ora che l’Unione Sovietica è defunta da quasi trent’anni — e vista la difficoltà nel ricevere opinioni di prima mano dalla classe operaia cinese sfruttata — molte persone in Nord America vivono il Socialismo autoritario come un concetto completamente astratto, distante dal loro vissuto quanto le esecuzioni di massa effettuate con la ghigliottina. Desiderando non solo la vendetta ma anche un <em>deus ex machina</em> che le salvi sia dall’incubo del capitalismo sia dalla responsabilità di crearne un’alternativa, immaginano lo Stato autoritario come un eroe che potrebbe combattere nel loro nome. Ricordiamo ciò che George Orwell scrisse nel suo saggio “Nel ventre della balena” riferendosi agli agiati scrittori stalinisti britannici degli anni Trenta:

<quote>

“Per uomini di questo genere, cose come epurazioni, polizia segreta, esecuzioni sommarie, galera senza processo, ecc., ecc., erano troppo remote per sembrare terrificanti. Essi possono mandar giù il totalitarismo perché non conoscono altro che il liberalismo.”

</quote>

*** Punire il colpevole

<quote>

“Credi in visioni che non presentino secchi di sangue.”

-Jenny Holzer

</quote>

In generale, tendiamo a essere più consapevoli dei torti commessi contro di noi rispetto a quelli che noi commettiamo nei confronti degli altri. Siamo più pericolosi quando sentiamo di aver subito un’ingiustizia, perché riteniamo di avere il diritto di giudicare, di essere crudeli. Quanto più ci sentiamo giustificati, tanto più dobbiamo stare attenti a non replicare i modelli dell’industria della giustizia, i presupposti dello Stato carcerario, la logica della ghigliottina. Ancora una volta, questo non giustifica l’indifferenza; vuol soltanto dire che, per non assumere il ruolo dei nostri oppressori, dobbiamo procedere in modo molto critico proprio quando ci sentiamo più giusti.

Quando ci immaginiamo combattere contro specifici esseri umani anziché contro dei fenomeni sociali, diventa più complicato riconoscere i modi in cui noi stessi siamo parte di quei fenomeni. Esternalizziamo il problema come qualcosa al di fuori di noi, personificandolo come un nemico che può essere sacrificato per la nostra purificazione simbolica. Tuttavia, quello che facciamo al peggiore di noi, alla fine verrà fatto al resto di noi.

Come simbolo di vendetta, la ghigliottina ci stuzzica facendoci immaginare di trovarci nei panni dei giudici, consacrati dal sangue degli empi. L’economia Cristiana della giustizia e della dannazione è essenziale per questo tableau. Al contrario, se utilizzata per simboleggiare qualcosa, la ghigliottina dovrebbe ricordarci che corriamo il rischio di diventare ciò che odiamo. La cosa migliore sarebbe essere in grado di combattere <em>senza odio,</em> perché crediamo in modo ottimistico nell’enorme potenziale dell’umanità.

Spesso, tutto ciò che serve per smettere di odiare una persona è riuscire a far sì che le diventi impossibile rappresentare qualsiasi tipo di minaccia per te. Quando qualcuno è già in tuo potere, è deplorevole ucciderlo. Questo è il momento cruciale per qualsiasi rivoluzione, quello in cui i rivoluzionari hanno l’opportunità di vendicarsi gratuitamente, di sterminare anziché semplicemente sconfiggere. Se non supereranno questo test, la loro vittoria sarà più ignominiosa di qualsiasi fallimento.

La peggior punizione che chiunque potrebbe infliggere a coloro che oggi ci governano e sorvegliano, sarebbe costringerli a vivere in una società in cui tutto ciò che hanno fatto è considerato imbarazzante — sedere in assemblee in cui nessuno li ascolta, continuare a vivere in mezzo a noi senza privilegi speciali pienamente consapevoli del male fatto. Se fantastichiamo su qualcosa, cerchiamo di farlo su come rendere i nostri movimenti così forti che difficilmente dovremo uccidere qualcuno per rovesciare lo Stato e abolire il capitalismo. Poiché sostenitori dell’emancipazione, questo si confà maggiormente alla nostra dignità.

È possibile impegnarsi nella lotta rivoluzionaria con tutti i mezzi necessari senza far diventare meschina la propria vita. È possibile rifuggire il bigotto moralismo pacifista senza sviluppare con ciò una cinica sete di sangue. Dobbiamo ampliare la capacità di esercitare la forza senza mai confondere il nostro vero obiettivo — ovvero creare collettivamente le condizioni per la libertà di tutti — con il potere sugli altri.

<quote>

“Che l’uomo sia sciolto dalla vendetta: è il ponte per la più alta speranza e un arcobaleno dopo lunghe tempeste.”

— Friedrich Nietzsche (non proprio un sostenitore dell’emancipazione, ma uno dei principali teorici dei pericoli legati alla vendetta)

</quote>

*** Invece della ghigliottina

Ovviamente, sarà inutile appellarsi alla natura migliore dei nostri oppressori fino a quando non saremo riusciti a render loro impossibile trarre beneficio dall’opprimerci. La domanda è <em>come</em> farlo.

Gli apologeti dei giacobini dissentiranno sostenendo che, date le circostanze, era necessario almeno <em>un po’</em> di spargimento di sangue per far progredire la causa rivoluzionaria. Praticamente ogni massacro rivoluzionario della Storia è stato giustificato dalla necessità – è così che le persone giustificano <em>sempre</em> i massacri. Anche se un po’ di spargimento di sangue <em>fosse</em> necessario, non ci sarebbero scuse per eleggere a valori rivoluzionari la sete di sangue e il diritto. Se desideriamo esercitare una forza coercitiva in modo responsabile quando non c’è altra scelta, dovremmo alimentare il disgusto nei suoi confronti.

La nostra causa è mai stata fatta progredire dagli omicidi di massa? Di sicuro, le relativamente poche esecuzioni <em>compiute</em> dagli anarchici — come le uccisioni degli uomini di Chiesa pro-fascisti durante la Guerra civile spagnola — hanno permesso ai nostri nemici di raffigurarci nel modo peggiore, anche se su di loro pesa la responsabilità di aver commesso almeno diecimila omicidi in più. Nel corso della Storia, i reazionari hanno sempre sottoposto, in malafede, i rivoluzionari a due pesi e due misure, perdonando lo Stato per aver ucciso milioni di civili mentre richiamavano all’ordine — anche solo per aver rotto una vetrina — chi si ribellava. La domanda non è se ci abbiano resi popolari, ma se abbiano un posto in un progetto di liberazione. Se cerchiamo la trasformazione anziché la conquista, dovremmo valutare le nostre vittorie secondo una logica diversa rispetto a quella della polizia e dei militari che affrontiamo.

Questa non è una discussione contro l’uso della forza. Si tratta piuttosto di capire come fare a impiegarla senza creare nuove gerarchie, nuove forme di oppressione sistematica.

L’immagine della ghigliottina è la propaganda per il tipo di organizzazione autoritaria che può avvalersi di quel particolare strumento. Ogni strumento implica le forme di organizzazione sociale necessarie per impiegarlo. Nel suo libro di memorie, <em>Bash the Rich</em> (Picchia il ricco), il veterano della guerra di classe Ian Bone cita John Barker, membro della Brigata degli Arrabbiati, secondo cui “le molotov sono molto più democratiche della dinamite,” suggerendo che dovremmo analizzare ogni strumento di resistenza per quanto riguarda il modo in cui struttura il potere. Criticando il modello di lotta armata adottato dai gruppi gerarchici autoritari in Italia negli anni ‘70, Alfredo Bonanno e altri insurrezionalisti hanno sottolineato che la liberazione poteva essere raggiunta solo attraverso metodi di resistenza orizzontali, decentralizzati e partecipativi.

<quote>

“Non è possibile fare la rivoluzione solo con la ghigliottina. La vendetta è l’anticamera della guida. Chi vuole vendicarsi ha bisogno di un capo. Un capo che conduca alla vittoria e ristabilisca la giustizia ferita.”

-Alfredo Bonanno, <em>Gioia armata</em>

</quote>

Se coesa, una folla in rivolta può difendere una zona autonoma o esercitare pressioni sulle autorità senza bisogno di una leadership gerarchica centralizzata. Laddove ciò diventasse impossibile — quando la società si è divisa in due parti distinte, pienamente preparate a massacrarsi l’un l’altra attraverso mezzi militari — non si potrebbe più parlare di rivoluzione, ma solo di guerra. La premessa alla rivoluzione è che la sovversione possa diffondersi attraverso le ostilità, destabilizzando posizioni fisse, minando le alleanze e le ipotesi alla base dell’autorità. Non dovremmo mai affrettarci a intraprendere il passaggio dal fermento rivoluzionario alla guerra. Questo, di solito, preclude le possibilità anzichè espanderle.

In quanto strumento, la ghigliottina dà per scontato che sia impossibile trasformare le proprie relazioni con il nemico, solo per abolirle. Inoltre, la ghigliottina presume che la vittima sia già completamente in potere di coloro che la impiegano. La ghigliottina — a differenza delle coraggiose azioni eroiche realizzate dalle persone durante le rivolte popolari nonostante le indicibili difficoltà — è un’arma per codardi.

Rifiutando di massacrare i nostri nemici in blocco, manteniamo aperta la possibilità che un giorno possano unirsi a noi nel nostro progetto di trasformazione del mondo. L’autodifesa è necessaria ma, ovunque si possa, dovremmo correre il rischio di lasciare vivi i nostri avversari. Non farlo ci assicura che nessuno di noi sarà meglio del peggiore tra loro. Dal punto di vista militare, questo è un handicap; ma se aspiriamo veramente alla rivoluzione, questo è l’unico modo.

*** Liberare, non sterminare

<quote>

“Ispirare speranza ai molti oppressi e paura ai pochi oppressori: questo è il nostro compito. Se riusciremo a dare speranza ai molti, i pochi finiranno per temere quella speranza. Noi però non vogliamo questo; non vogliamo vendetta per i poveri, ma felicità; e poi, quale vendetta mai li risarcirebbe delle migliaia di anni di sofferenze patite?”

— William Morris, “How We Live and How We Might Live” (Come potremmo vivere)

</quote>

Quindi ripudiamo la logica della ghigliottina. Non vogliamo sterminare i nostri nemici. Non pensiamo che il modo per creare armonia sia eliminare dalla faccia della terra tutti coloro che non condividono la nostra ideologia. Noi desideriamo un mondo che contenga molti mondi, come diceva il subcomandante Marcos – un mondo in cui l’unica cosa impossibile sia dominare e opprimere.

L’anarchismo è una proposta per <em>tutti,</em> relativa al modo in cui potremmo migliorare la nostra vita: lavoratori e disoccupati, persone di tutte le etnie e generi e nazionalità o la mancanza della stessa, poveri e miliardari. La proposta anarchica non è nell’interesse di un gruppo attualmente esistente contro un altro: non è un modo per arricchire i poveri a spese dei ricchi, o per potenziare un’etnia, una nazionalità o una religione a spese altrui. L’intero modo di pensare fa parte di ciò da cui stiamo cercando di scappare. Tutti gli “interessi” che, presumibilmente, caratterizzano diverse categorie di persone sono prodotti dell’ordine dominante e devono essere trasformati insieme a esso, non conservati o assecondati.

Dal nostro punto di vista, anche le posizioni più elevate di ricchezza e potere disponibili nell’ordine esistente sono prive di valore. Niente di ciò che il capitalismo e lo Stato possono offrire hanno valore per noi. Proponiamo una rivoluzione anarchica sulla base del fatto che questa potrebbe finalmente soddisfare i desideri che l’ordine sociale prevalente non potrà mai soddisfare: il desiderio di essere in grado di provvedere a se stessi e ai propri cari senza farlo a spese di qualcun altro, il desiderio di essere valutati per la propria creatività e il proprio carattere anziché per quanto si guadagna, il desiderio di strutturare la propria vita attorno a ciò che è profondamente gioioso anziché assecondando gli imperativi della competizione.

Ciò che noi suggeriamo è che tutti coloro che vivono oggi potrebbero andare d’accordo – se non <em>bene,</em> almeno <em>meglio</em> – se non si fosse costretti a competere per il potere e per le risorse nei giochi a somma zero di politica ed economia.

Lascia agli antisemiti e agli altri estremisti descrivere il nemico come un certo <em>tipo</em> di persone, per personificare tutto ciò che temono come l’altro. Il nostro avversario non è una tipologia di essere umano, ma la forma di relazioni sociali che impone l’antagonismo tra le persone come modello fondamentale per politica ed economia. Abolire la classe dirigente non significa ghigliottinare tutti coloro che oggi possiedono uno yacht o un attico; significa rendere impossibile a chiunque esercitare sistematicamente il potere coercitivo su chiunque altro. Non appena ciò sarà impossibile, nessuno yacht o attico rimarranno vuoti a lungo.

Per quanto riguarda i nostri avversari diretti — quegli esseri umani determinati a mantenere l’ordine prevalente a tutti i costi —, ciò a cui noi aspiriamo è la loro sconfitta, non il loro sterminio. Per quanto egoisti e avidi possano apparire, almeno alcuni dei loro valori sono simili ai nostri e la maggior parte dei loro errori — come i nostri — derivano dalle loro paure e debolezze. In molti casi, si oppongono alle proposte della sinistra proprio a causa di ciò che è internamente incoerente in loro — come, per esempio, l’idea di realizzare la comunione dell’umanità mediante la coercizione violenta.

Anche quando siamo impegnati in una dura lotta fisica con i nostri avversari, dovremmo mantenere una profonda fiducia nelle loro potenzialità, perché speriamo che, un giorno, potremo avere con loro una relazione differente. In quanto aspiranti rivoluzionari, questa speranza è la nostra risorsa più preziosa, il fondamento di tutto ciò che facciamo. Se il cambiamento rivoluzionario deve diffondersi in tutta la società e in tutto il mondo, quelli che combattiamo oggi dovranno combattere al nostro fianco domani. Non predichiamo la conversione con la spada, né immaginiamo di persuadere i nostri avversari in un mercato astratto d’idee; miriamo piuttosto a interrompere i modi in cui capitalismo e Stato si riproducono attualmente, dimostrando, al contempo, i vantaggi della nostra alternativa in modo inclusivo e contagioso. Non ci sono scorciatoie quando si tratta di cambiamenti duraturi.

Proprio perché, a volte, nei nostri conflitti con i difensori dell’ordine prevalente è necessario impiegare la forza, è particolarmente importante non perdere mai di vista le nostre aspirazioni, la nostra empatia e il nostro ottimismo. Quando siamo costretti a usare la forza coercitiva, l’unica giustificazione possibile è che si tratti di un passo necessario verso la creazione di un mondo migliore <em>per tutti,</em> compresi i nostri nemici o, perlomeno, i loro figli. Altrimenti, rischiamo di diventare i prossimi giacobini, i prossimi profanatori della rivoluzione.

<quote>

“L’unica vera vendetta che potremmo avere sarebbe attraverso i nostri sforzi per arrivare alla felicità.”

— William Morris, in risposta alle richieste di vendetta per gli attacchi della polizia alle manifestazioni a Trafalgar Square.

</quote>

*** Appendice: i decapitati

La carriera della ghigliottina non giunse al termine né con la fine della prima Rivoluzione francese, né quando fu bruciata durante la Comune di Parigi. In Francia fu, infatti, utilizzata fino al 1977 come mezzo statale per eseguire la pena capitale. Una delle ultime donne lì <strong>ghigliottinate</strong> fu giustiziata per aver procurato aborti. I nazisti ghigliottinarono circa 16.500 persone tra il 1933 e il 1945, lo stesso numero di persone uccise durante l’apice del terrore in Francia.

Qualche vittima della ghigliottina:

 - Ravachol (nato François Claudius Koenigstein), anarchico

 - Auguste Vaillant, anarchico

 - Emile Henry, anarchico

 - Sante Geronimo Caserio, anarchico

 - Raymond Caillemin, Étienne Monier e André Soudy, tutti anarchici membri della cosiddetta Banda Bonnot

 - Mécislas Charrier, anarchico

 - Felice Orsini, che tentò di assassinare Napoleone III

 - Hans e Sophie Scholl e Christoph Probst – membri de La Rosa Bianca, un’organizzazione giovanile antinazista sotterranea, attiva a Monaco dal 1942 al 1943.

<quote>

“Io sono un anarchico. Siamo stati impiccati a Chicago, giustiziati sulla sedia elettrica a New York, ghigliottinati a Parigi e strangolati in Italia, e io andrò con i miei compagni. Io mi oppongo al vostro Governo e alla vostra autorità. Sto con loro. Fate del vostro peggio. Lunga vita all’Anarchia.”

— Chummy Fleming

</quote>

*** Ulteriori letture

The Guillotine At Work (La ghigliottina al lavoro: venti anni di terrore in Russia), GP Maximoff

Io so chi ha ucciso il commissario Luigi Calabresi, Alfredo M. Bonanno

Critique’s Quarrel with Church and State (Il conflitto di critica con la Chiesa e lo Stato), Edgar Bauer

[1] Come riportato nella Gazzetta ufficiale della Comune di Parigi:
<br>
“Giovedì, alle nove del mattino, il 137º battaglione, appartenente all’XI arrondissement, si è diretto in Rue Folie-Mericourt; hanno richiesto e preso la ghigliottina, hanno fatto a pezzi la macchina orribile e l’hanno bruciata applauditi da una folla immensa.
<br>
“L’hanno bruciata ai piedi della statua del difensore di Sirven e Calas, l’apostolo dell’umanità, il precursore della Rivoluzione francese, ai piedi della statua di Voltaire.”
<br>
Ciò era stato annunciato in precedenza nel seguente proclama:
<br>
“Cittadini,
<br>
“Siamo stati informati della costruzione di un nuovo tipo di ghigliottina che è stato commissionato dall’esecrabile Governo [ovvero, il Governo repubblicano conservatore sotto Adolphe Thiers] – un modello più veloce e più facile da trasportare. Il Sottocomitato dell’XI arrondissement ha ordinato il sequestro di questi servili strumenti di dominio monarchico e ha votato per la loro distruzione una volta per tutte. Saranno quindi bruciati alle 10 del 6 aprile 1871, in Place de la Mairies, per la purificazione dell’arrondissement e la consacrazione della nostra nuova libertà.”

[2] Come abbiamo sostenuto altrove, feticizzare “lo stato di diritto” spesso serve a legittimare atrocità che altrimenti verrebbero percepite come orribili e ingiuste. La Storia continua a mostrare come il Governo centralizzato possa perpetrare la violenza su una scala molto più grande di qualsiasi cosa insorga nel “caos non organizzato.”

[3] Almeno un collaboratore della rivista <em>Jacobin</em> ha, disgustosamente, persino tentato di riabilitare questo precursore dei peggiori eccessi dello stalinismo, fingendo che una religione autorizzata dallo Stato potesse essere preferibile all’ateismo autoritario. L’alternativa alle religioni autoritarie e alle ideologie autoritarie che promuovono l’islamofobia e simili non è uno Stato autoritario che imponga una religione propria, bensì uno Stato che costruisca la solidarietà di base attraverso linee politiche e religiose in difesa della libertà di coscienza.
