#title Della tolleranza
#author Camillo Berneri
#LISTtitle Della tolleranza
#SORTauthors Berneri, Camillo;
#SORTtopics anarchismo, tolleranza, religione
#date 1924
#source Consultato l’8 febbraio 2018 su [[https://www.panarchy.org/berneri/tolleranza.html][www.panarchy.org]]
#lang it
#pubdate 2018-02-08T16:02:52



La coscienza relativistica della verità e del bene, se porta a guardare le cose da un angolo visuale più largo e conduce, quindi, alla tolleranza, porta ad un pericolo: lo scetticismo. Minaccia di indebolire la molla dell’azione, specie di quella implicante il sacrificio. Bianco o nero: bisognerebbe vedere così, per lottare senza incertezze; per dare alla nostra volontà una direzione rettilinea. Ma questa posizione non è possibile nella nostra epoca, in cui lo spirito critico s’è affilato e in cui la vita è complessa; per l’incrociarsi delle varie correnti ideologiche e trasmutare di valori morali, per il poliedrico aspetto dei problemi di vita politica, economica, sociale.

La tolleranza, del resto, non implica scettica valutazione della vita; dubbio sui fini e sui metodi. E non giustifica il ritrarsi egoistico dell’opera comune. Né implica tolstoiana rinuncia alla violenza.

Tolleranza vale: coscienza del processo relativistico della verità, che non è un <em>quid</em> assoluto anteposto all’errore, ma il passaggio da una ad un’altra verità; un divenire. La verità è un momento dell’errore, e viceversa. La verità, quindi, non è A o B, ma la negazione di uno dei due termini, per il principio di contraddizione. Processo di negazione – affermazione che costituisce il progresso intellettuale. Ma tutto questo vale nella metafisica. Nella vita vi sono delle verità assolute come sono quelle che rampollano dal sentimento. Sono quelle ragioni che la ragione non conosce, delle quali ci parla il Pascal. Nella vita la verità è ciò che si crede vero. È ciò che serve di punto di appoggio alla ragione, di stimolo e di conforto al sentimento; di leva all’azione. Verità è per me, ad esempio, il dovere della ribellione contro l’ingiustizia sociale e l’oppressione politica. Di questa verità sono certo, perché ne sento l’impeto e la bellezza.

La tolleranza ha, dunque, due piani di possibilità: quello intellettuale e quello morale. Quanto al primo è tollerante colui che conoscendo il valore dello scambio di idee, della loro fusione o contrasto, non respinge aprioristicamente le ideologie altrui, ma si accosta ad esse e tenta penetrarle; per trarne ciò che vi è di buono. Questa tolleranza è abbastanza frequente fra le persone colte e chi prova l’assillo del pensiero riesce ad acquistarne l’abito. La naturale conseguenza di questa tolleranza sarà il rispetto per qualsiasi espressione di qualsiasi credo religioso, filosofico, estetico.

Quanto al secondo è tollerante colui che, pur avendo fede in un gruppo di principi e sentendo profondamente la passione di parte, comprende che altri, per il loro carattere, per l’ambiente in cui vivono, per l’educazione ricevuta, ecc., non partecipa alla sua fede e alla sua passione. La distinzione tra il male e il malvagio, tra la tirannide e gli oppressori è scolastica, e chi concepisce la vita come lotta per il bene e per la libertà deve combattere coloro che intralciano la sua opera di redenzione. Ma il suo spirito, pur negando come formalistica la distinzione sopracennata nei riguardi del problema morale dell’azione, giunge a combattere senza l’odio bruto che non sa la pietà e non aspira ad un mondo in cui la violenza non sia più necessaria.

Tolleranza, dunque, non è scetticismo intellettuale né apatia morale.

Parrà ad alcuno che, dati i tempi che corrono e data la nostra condizione di vinti, sia inutile e fors’anche fuori di luogo il trattare della tolleranza. Mi pare, invece, proprio questo il momento opportuno. L’intolleranza degli altri ci mostra la sua faccia briaca. Guardiamola, prima che la bufera trascini anche noi.

I fascisti che bruciano i giornali di opposizione sono, per lo più quegli stessi sovversivi che non leggevano che i giornali del proprio partito e ci giuravano sopra.

I fascisti che fanno a pezzi le bandiere rosse sono, per lo più, quelli che non volevano che i preti suonassero le campane, che disturbavano le processioni, che offendevano gli ufficiali, ecc. Là dove l’ineducazione sovversiva era maggiore il fascismo s’è sviluppato prima e più largamente. Perché l’intolleranza della violenza spicciola è il portato della miseria e grettezza intellettuale e di una scarsa e deviata sensibilità morale.

Che cosa hanno fatto i dirigenti dei partiti di sinistra per combattere l’intolleranza bruta? Ben poco. Erano quasi tutti tribuni.

E il tribuno è il servo della folla.

L’intolleranza cieca e brutale ha disperso in mille sensi l’energia aggressiva delle avanguardie. Invece di concentrarsi sui punti vitali delle difese borghesi e statali s’è divisa e suddivisa in piccole azioni sporadiche. Piccoli fuochi di paglia, bastanti a svegliare il cane di guardia ed insufficienti a dar fuoco alla casa. Bisogna che i rivoluzionari coscienti non si lascino intenerire dalle violenze inutili, dalle malvagità. La rivoluzione è una guerra, e chi l’accetta non può perdersi dietro all’episodio singolo. Ma in un periodo pre-rivoluzionario è necessario che la tolleranza dei coscienti costringa per quanto può la violenza acefala nei limiti di un’azione diretta contro nemici reali e in un periodo post-rivoluzionario è necessario che i tolleranti intervengano contro le inutili e vili rappresaglie, che servirebbero di pretesto alla dittatura.

Anche riguardo alla tolleranza il giusto morale e l’utile politico concordano.

E a svolgere quest’azione di tolleranza, con la propaganda e con la forza, dobbiamo essere noi. I comunisti hanno una mentalità domenicano-giacobina, i socialisti riformisti sono dei De Amicis che si perdono in un impotente sentimentalismo. Noi possiamo abbinare la violenza e la pietà, in quell’amore per la libertà che ci caratterizza politicamente ed individualmente.

La tolleranza è un concetto squisitamente nostro, quando non si intenda con questo termine il menefreghismo.

L’anarchia è la filosofia della tolleranza.

La pietà verso chi delinque è il substrato della nostra negazione del diritto penale.

Il nostro internazionalismo è basato sul principio della possibilità di pacifica convivenza di vari gruppi etnici aventi una lingua, una storia, usi, costumi diversi. Così la nostra concezione di assoluta libertà di stampa, di parola, d’insegnamento è basata sulla convinzione che non siano dannose varie e contrastanti correnti di pensiero, quando queste si correggano reciprocamente nel libero gioco della loro concorrenza. Anche nel campo economico, la nostra tolleranza si afferma, riguardo all’artigianato di fronte alla grande industria, alla piccola proprietà rurale di fronte all’agricoltura collettiva. Noi siamo i liberisti del socialismo appunto per questa fiducia nella possibilità di fusione degli estremi, di soluzione armonica degli opposti. E per il senso dinamico della vita, che alla rigida uniformità ci fa preferire l’infinita varietà e negli uomini e nelle cose.

La nostra intolleranza (<em>violenza</em>) è concepita e sentita come condizione necessaria della più ampia tolleranza. Respinta la società dal campo delle competizioni egoistiche, e tragiche per la loro necessità, in quello più ampio dei contrasti ideologici, spirituali, noi crediamo sarà realizzata quella città che oggi pare utopistica: la città del buon accordo.

Non si uccide per un pezzo di pane tra satolli. Non si ucciderà per dissidi ideali in una società che assicura il benessere materiale, che non minaccia la vita dei suoi membri, che permette loro di raggiungere quel livello spirituale, a cui siamo giunti fino da ora quasi tutti, all’altezza del quale la violenza ripugna e il rispetto è possibile.

Le lotte religiose furono sanguinose in secoli di miseria e di tenebre. Oggi non lo sono più. E là dove lo sono, come nell’Irlanda e nell’India, al fanatismo s’innesta determinante ambientale del primo, la ragione economica; sotto forme politico-sociali.

L’anarchia non sarà la società dell’armonia assoluta, ma la società della tolleranza.

Ma l’anarchia, come ammoniva giustamente il Fabbri, non diviene per una specie di fatalità storica. Diverrà se la vorremo, fin d’ora, con chiarezza di pensiero e costanza di volontà. Se la costruiremo in noi e negli altri, giorno per giorno: con la propaganda e con l’azione nella quale dovrebbe avere il primo posto l’esempio di coerenza.

E a proposito di coerenza credo che sia un nostro pericolo quello dell’intolleranza della tolleranza. Non è un bisticcio di parole. In quest’errore è caduto il Rousseau quando nel <em>Contratto Sociale</em> scrive: «Bisogna senza pietà bandire dalla Repubblica tutti i settari che dicono: non v’è salvezza fuori dalla nostra chiesa; perché siffatta intolleranza in materia di dogma porta con sé necessariamente l’intolleranza in materia civile, l’ineguaglianza, l’ingiustizia e le discordie. Lo Stato non dovrà accettare fra i suoi membri, che quelli che aderiranno a questo Credo morale e sociale; esso punirà con le più gravi pene, anche con la morte, chiunque, dopo averlo accettato, lo rinnegherà con la parola o con la condotta».

Nessuno di noi arriverebbe a questo punto. Ma su questa strada alcuni ci sono, specie per quanto riguarda la religione.

Ci sono delle persone religiose che nutrono simpatia per il nostro programma politico-sociale, ma che arrivano ad esso partendo da presupposti di carattere etico-religioso. Ebbene io dubito che esse potrebbero, senza trovarsi a disagio, far parte della nostra famiglia politica. Esse sarebbero <em>intolleranti</em>, riguardo alla religione. Vale a dire non si conformerebbe all’ateismo della maggioranza e cercherebbero di convertire il maggior numero di compagni. Io credo che in questo non ci sarebbe gran che di male. Perché convertirebbero pochi e quei pochi rimarrebbero, se lo sono, dei buoni compagni.

A questo punto qualcuno protesterà. È per quelli che non sono d’accordo con me che ho scritto questo articolo.



