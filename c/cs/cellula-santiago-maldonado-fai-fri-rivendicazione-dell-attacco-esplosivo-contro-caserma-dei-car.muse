#title Rivendicazione dell’attacco esplosivo contro caserma dei carabineri (07/12/2017)
#author Cellula Santiago Maldonado FAI/FRI
#LISTtitle Rivendicazione dell’attacco esplosivo contro caserma dei carabineri (07/12/2017)
#SORTauthors Cellula Santiago Maldonado FAI/FRI
#SORTtopics rivendicazione, azione diretta, FAI/FRI, polizia
#date dicembre 2017
#source Consultato il 20 febbraio 2018 su [[https://anarhija.info/library/roma-italia-cellula-santiago-maldonado-fai-fri-rivendica-l-attacco-esplosivo-contro-c-it][anarhija.info]]
#lang it
#pubdate 2018-02-21T23:17:48



In tempi di pace sociale e di attendismo non c’è migliore risposta che l’azione. Uno stimolo, una continuità e uno scossone per svegliare chi dorme.

Agire di propria iniziativa rompe l’attendismo e l’immobilità e incendia coloro a cui ribolle il sangue.

La prassi anarchica dell’attacco deve essere lo stimolo base dell’anarchia, altrimenti è un morto che cammina. Un agire necessario a renderci vivi nei modi che riteniamo opportuni, fuori da ogni programma, struttura gerarchica e verticistica. Una tante pratiche rivoluzionarie sono parte dell’anarchismo nelle sue viscere.

Abbaiamo deciso di prendere in mano la nostra vita rompendo la pace opprimente che ci circonda.

La notte del 6/7 dicembre è stato collocato nella caserma dei carabinieri del quartiere San Giovanni, Roma, termos acciaio con 1,6 kg di esplosivo.

Le nostre attenzioni si sono riversate verso i principali tutori dell’ordine mortifero del capitalismo: le forze dell’ordine. Senza di esse i privilegi, le prepotenze, le ricchezze accumulate dai padroni sarebbero nulla. Perche’ hanno da sempre la funzione di reprimere, incarcerare, deportare, torturare, uccidere chi per scelta o necessita si ritrova al di fuori della loro legge.

La lotta contro lo Stato non e’ semplice e non si riduce a formule magiche. Ma gli obbiettivi ci sono e non si possono sempre fare teorie e chiacchiere di convenienza. Ogni individuo libero per desiderio e necessità mette in campo l’azione, qui ed ora. Non c’è delega nella lotta per la libertà.

Non bisogna lasciarsi prendere dallo sconforto che questi tempi instillano a dosi massicce.

Cosa sarebbero stati questi anni se una minoranza di refrattari non avesse preso in mano la fiaccola dell’anarchia? Se queste/i compagne/i avessero aspettato tempi migliori? Ne sa qualcosa l’allora presidente della Commissione Europea a cui è stato rovinato il Natale. Ne sa qualcosa il vampiro di equitalia, mutilato di alcuni dei suoi artigli. Deve averlo sentito forte alle gambe il calore della fiaccola dell’anarchia lo stregone dell’atomo dell’Ansaldo Nucleare.

Oggi sia noi a prendere in mano la fiaccola dell’anarchia, domani sarà qualcun altro. Purché non si spenga!

Chi vuole guardare resterà a guardare. Chi non vuole agire giustificandosi politicamente, continuerà a non farlo. Non stiamo aspettando alcun treno della speranza, non aspettiamo tempi maturi. Le condizioni si muovono con lo scontro. Il movimento è tale se agisce, se no sta fermo. L’emancipazione dell’individuo dall’autorità e della sfruttamento è fatta dai diretti interessati.

Eppure chi attacca contagia chi sente una pulsione. Questo vuol dire propaganda col fatto.

Contro sbirri, politici e loro tirapiedi. Contro ingegneri della scienza e dell’industria. Contro tutti i padroni, ma anche contro tutti i servi. Contro la schiera dei cittadini onesti della società-galera.

Non ci interessa perdere tempo ed energie nella critica dei riformisti... Pur non considerandoci una minoranza elitaria, come anarchici abbiamo le nostre azioni e le nostre rivendicazioni. La nostra propaganda. Ogni individuo e gruppo di affinità sviluppa e accresce le proprie esperienze nel legame fraterno. Senza alcuno specialismo e senza voler imporre un metodo. Noi abbiamo scelto questo. Che ognuno trovi la sua strada nell’azione. L’organizzazione gerarchica strutturata oltre a uccidere la libertà dei singoli, rende anche più esposti alla reazione della repressione.

L’organizzazione anarchica informale è lo strumento che abbiamo ritenuto più opportuno in questo momento, per questa specifica azione, perché ci permette di tenere insieme la nostra irriducibile individualità, il dialogo attraverso la rivendicazione con gli altri ribelli, e infine la propaganda veicolata dall’eco dell’esplosione.

Non è non vuole essere uno strumento assoluto e definitivo.

Un gruppo di azione nasce e si sviluppa sulla conoscenza, sulla fiducia. Ma altri gruppi e singoli possono condividere, anche solo temporaneamente, una progettualità, un dibattito, senza conoscersi di persona. Si comunica direttamente attraverso l’azione.

L’azione diretta distruttiva è la risposta elementare di fronte alla repressione. Ma non solo. La prassi anarchica è anche un rilancio, una proposta che va oltre la solidarietà, rompendo la spirale repressione-azione-repressione. Le azioni di solidarietà sono importanti, ma non possiamo rinchiuderci nella critica, per quanto armata, di qualche operazione repressiva o di qualche processo.

I/le compagni/e prigionieri/e sono parte della lotta, ci danno fianco e ci danno forza. Ma è necessario agire e organizzarsi. L’avanzare dello sviluppo tecnologico, le politiche di controllo e repressione non danno molti margini di valutazione sul che fare. Si sta ridisegnando la vita e la repressione nelle metropoli. Muoversi, agire, può diventare sempre più complicato.

Al contrario degli “scontri” spesso preannunciati da una certo antagonismo, l’imprevedibilità è l’arma migliore contro la società del controllo. Colpire dove non ti aspettano. Oggi colpiamo nel cuore della capitale militarizzata per sfidare i deliri securitari. Domani chissà, magari in periferia dove non immaginate. Noi dare tregue, ma scegliere noi i tempi. E’ da sempre il principio della guerriglia metropolitana. Con la differenza che la cospirazione delle cellule informali non conosce gerarchie e direzioni strategiche. E per questo è ancora meno prevedibile.

Lo stato italiano è all’avanguardia delle politiche repressive e militari. Per collocazione geografica si trova spesso a fare il lavoro sporco per difendere i confini della fortezza europea.

I recenti accordi del ministro Minniti coi sanguinari colonnelli libici ne sono la prova recente. Raggiunto il numero di schiavi necessari “sfruttiamoli a casa loro” oltre ad essere popolare è pur sempre un buon affare.

La notte scorsa abbiamo portato la guerra a casa del ministro Minniti. I diretti responsabili in divisa, coloro che obbediscono tacendo e tacendo crepano, hanno ricevuto un assaggio di quello che si meritano.

Con questa azione lanciamo una campagna internazionale di attacco contro uomini, strutture e mezzi della repressione. Ognuno con lo strumento che ritiene più opportuno e se lo desidera contribuendo al dibattito.

      *FEDERAZIONE ANARCHIA INFORMALE – FRONTE RIVOLUZIONARIO INTERNAZIONALE*

      *Cellula Santiago Maldonado*

*Dedichiamo questa azione all’anarchico argentino rapito e assassinato dai sicari della Benetton. Che venga presto il giorno in cui a sparire dalla faccia della terra finalmente saranno gli oppressori.*



