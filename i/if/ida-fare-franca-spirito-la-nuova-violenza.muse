#title La nuova violenza
#author Ida Faré, Franca Spirito
#LISTtitle Nuova violenza
#SORTauthors Faré, Ida; Spirito, Franca;
#SORTtopics femminismo, anni ’70, femminismo, aborto, azione diretta
#date 1979
#source Consultato il 21/11/2021 su [[https://web.archive.org/web/20041108184641/[[http://www.anarcotico.net/index.php?module=pagesetter&func=viewpub&tid=13&pid][anarcotico.net]]
#lang it
#pubdate 2021-11-21T23:00:00
#notes Nota di anarcotico.net:
<br>
Questo breve opuscolo è l’ultimo capitolo del libro <em>Mara e le altre</em> scritto da Ida Faré e Franca Spirito nel 1979. Pur non condividendo l’impostazione del libro, un’analisi psico-sociologica sulle “protagoniste” della lotta armata, riteniamo che questo capitolo possa far conoscere fatti ormai dimenticati e fornire spunti per la riflessione e per l’azione.
<br>
Dedichiamo questo opuscolo ad Ann Hansen, militante del gruppo rivoluzionario canadese degli anni 80 “Direct Action”. Per le sue azioni contro il sessismo, l’industria bellica ed in difesa della terra ha passato venti anni nelle carceri canadesi.

<quote>

<strong>Luisa Spagnoli, una solerte aguzzina</strong> arricchita con lo sfruttamento bestiale del lavoro delle detenute proletarie.Una donna sensibile che “veste” con molto buon gusto le signore dell’alta borghesia con abiti da L. 150.000 e golf da L. 20.000 che paga alle detenute rispettivamente L. 3.000 e L. 1.500 sfruttandole per dodici ore e più al giorno con lavorazioni a cottimo.
<br>
Oggi due boutiques della signora Spagnoli hanno chiuso in perdita : due gruppi di compagne le hanno bruciate…; La signora Spagnoli, tutti gli sfruttatori, tutti gli sbirri, i porci, le suore e i collaborazionisti dovranno d’ora in poi fare i conti con la coscienza rivoluzionaria del movimento delle donne.

</quote>
<quote>

<strong>Perché è saltata la macchina di Luigi D’Incerto Bonino?</strong>
<br>
per chiarire chi è Luigi D’Incerto Bonino bastano queste poche note:
<br>
F.C., casalinga , due figli, malata di nefrite cronica (la malattia ha già fatto una vittima ,il primo figlio della donna è deforme). Quando si accorge di una nuova gravidanza, F.C. con l’assenso di ben due ginecologi, si reca al reparto di ginecologia di Niguarda di cui è primario Luigi D’Incerto Bonino, per avere l’aborto terapeutico. “Questo certificato è carta straccia”, si sente dire dal primario. “Proibisco che nel mio ospedale si facciano aborti. Se qualcuno dei miei assistenti vuole farlo, lo faccia a casa sua”. Sempre dalla stampa apprendiamo che ha deferito all’Ordine dei medici una sua assistente che ha disobbedito operando una donna molto malata… Da simili loschi figuri e da mille altri che popolano gli ospedali gestendoli come loro feudi e che usano la medicina come strumento di potere e di denaro non è certo la legge a difenderci. E’ solo la nostra disorganizzazione che ne permette l’esistenza, ma è solo questione di tempo! Contro di loro saremo capaci di tanta forza e creatività da distruggerli insieme alla loro scienza di parte… Speriamo che questo non rimanga un gesto singolo ma che altre donne si organizzino per annientare tutti quegli individui e istituzioni che le opprimono impedendo loro di vivere una vita decente.

</quote>
<quote>

<strong>Oggi due marzo 1977, un gruppo di donne ha attentato nel Sacro Cuore dell’Università Cattolica di Milano, al Rettorato.</strong> Il Magnifico Rettore G. Lazzati noto crociato antiabortista si è messo in luce per il solerte lavoro di organizzazione dell’obiezione di coscienza dei medici… Oggi le streghe non stanno ad aspettare il rogo: questa volta il fuoco lo appicchiamo noi!

</quote>
<quote>

<strong>Oggi 8 marzo 1977 un gruppo di donne ha colpito uno dei personaggi più infami e responsabili della violenza sulle donne messosi tristemente in luce nel dramma di Seveso</strong>: il prof. G. Amico, primario neurologico dell’Ospedale di Desio, psichiatra con parere decisionale della commissione per l’aborto terapeutico. Il sadismo di questo individuo non ha bisogno di commenti. La tragedia di Seveso è per lui solo un problema legale-tecnico: di fronte a donne distrutte dalla paura, dal dolore, dai sensi di colpa che tabù secolari alimentano si è permesso di dire: “Signora dia retta a me, è meglio un figlio handicappato che uno sano che poi magari diventa tossicomane.Se il bambino nasce malato lo porti pure qui, lo mettiamo in un istituto e faremo avere un sussidio ai genitori”…
<br>
Alla barbarie delle affermazioni fa seguito una pratica degna di un nazista… Ad una donna ha fatto ascoltare il battito del feto, ad un altro gruppo di donne tenute in osservazione trasmettono tramite altoparlanti la predica domenicale antiabortista pronunciata durante la messa…
<br>
Ma stiano attenti questi mostri, questi baroni onnipotenti cui la legge permette tutto e delega tutto: il movimento delle donne si sta organizzando e saprà come distruggerli insieme a tutte quelle istituzioni che li proteggono… Non è vero che la violenza è estranea alle donne, da sempre la subiscono! Si tratta della violenza con cui ci hanno espropriato di tutto: corpo, mente, affetti; vita è la paura che ci ha fatto accettare di vivere di rinunce. Rompiamo questa violenza su di noi per arrivare ad esercitare una violenza finalmente liberatoria, una capacità offensiva che è l’unico mezzo per rompere questo cerchio di oppressione che ci circonda, per potere cominciare finalmente a vivere i nostri desideri…

</quote>
<quote>

<strong>Un’unità di donne combattenti per il comunismo attacca e perquisisce la sede della Mondial Lus.</strong> Si tratta di una ditta la cui padrona è una donna che fonda lo sviluppo del proprio profitto sulla pelle delle lavoratrici a domicilio organizzate in piccoli gruppi e su una mole significativa di lavoro nero nelle carceri e nei manicomi… Ci mettiamo oggi in prima fila tra le forze di classe nell’attacco, nel combattimento contro il potere nemico per la distruzione dei rapporti sociali che la società ci impone… Ci autodeterminiamo come soggetti della nostra liberazione nel progetto generale di distruzione dello Stato… Liberiamo le nostre forze contro chi ci vuole sottomesse: non schiave del nuovo comando nemico, non moderni angeli del focolare…

</quote>
<quote>

<strong>“Abbiamo bruciato la macchina del dott. Felice Basile”</strong> annuncia una voce femminile, sabato 23 luglio 1978 in una telefonata alla redazione torinese de La Stampa, “Perché è un obiettore di coscienza antiabortista. In una cabina in corso Ferrucci troverete un comunicato…”
<br>
Nel comunicato si legge “Questa è la prima risposta delle donne stufe della violenza dei macellai…”

</quote>

I volantini di cui abbiamo riportato alcuni stralci e che hanno rivendicato le rispettive azioni, sono firmati: “Violenza femminista”, “Donne combattenti per il comunismo”, “Alcuni collettivi femministi”, oppure semplicemente con uno slogan: “Organizziamoci contro il potere nemico”, “Streghe fuori, streghe dentro, siamo tutte nel movimento”, “Bruciamo i covi della nostra oppressione”.

Le azioni sono state compiute principalmente a Milano,Torino, Padova, Genova.

L’ultima è stata compiuta a Bergamo nelle notti del 30 e 31 ottobre 1978, quando due bombe ad alto potenziale sono state fatte esplodere davanti ai negozi People e Charlie Brown, simboli del “consumismo” e della “mercificazione del corpo della donna”. La mattina del 31 ottobre una voce femminile aveva rivendicato gli attentati con una telefonata ad un redattore di Radio Bergamo a nome delle “proletarie combattenti per il comunismo”.

In uno dei due negozi, il Charlie Brown, fin dal mese di settembre il proprietario Andrea Lodetti, aveva avuto l’idea di mettere in vetrina, per attirare l’attenzione dei passanti, tre ragazze in salopette (la tuta con pettorina e bretelle), ma senza camicia e dunque a seno mezzo nudo. Le ragazze poi, per accentuare un richiamo sessuale, mangiavano banane. Alcuni collettivi femministi avevano organizzato una manifestazione davanti ai negozi e in seguito all’intervento della polizia, era nato un tafferuglio: tre ragazze erano state denunciate per resistenza e oltraggio. Qualche giorno dopo gli attentati.

Tutti questi episodi mostrano l’esistenza e la crescita di azioni dirette e violente,condotte e rivendicate da gruppi di sole donne. Una scelta più o meno diffusa,più o meno organizzata, ma comunque ben diversa da quella delle donne che fanno parte dei gruppi armati “misti” come le Br e i Nap.

E’ difficile considerare la consistenza di questi gruppi che sfuggono a qualsiasi sommaria classificazione, cosi a qualsiasi radiografia o mappa, del resto necessariamente imprecisa o poliziesca. Non si tratta infatti di gruppi organizzati in modo costante, con una vita politica legata esclusivamente alla preparazione e alla messa in opera delle azioni, quanto piuttosto di gesti ed azioni sporadici e spontanei, di gruppi che si coagulano, nascono e si organizzano in occasione di un determinato e specifico obiettivo, per poi sciogliersi. E tutto questo è dimostrato, tra l’altro, anche dalla varietà e dalla non continuità delle firme, molte delle quali sono addirittura degli slogan e come tali intendono rappresentare uno stato d’animo di ribellione piuttosto che un’avanguardia costituita di donne armate. Si tratta dunque di capire, al di là di impossibili classificazioni e indagini, la vita politica che ci sta sotto.

Abbiamo parlato con alcune di queste donne, abbiamo avuto con loro alcuni confronti e discussioni, che tuttavia non abbiamo potuto riportare sotto forma di interviste. L’assenza di “parola diretta” è dovuta, infatti proprio alle caratteristiche di fluttuazione e mancanza di costituzione stabile di queste azioni: nessuna donna si sentiva di esprimerle o rappresentarle con una teoria o a nome di un gruppo. Abbiamo rispettato questa obiezione e non ci è rimasto che riportare quello che di loro abbiamo capito come tracce di un problema e di un discorso, aperto. E precisamente ci soffermiamo sugli aspetti che riguardano il rapporto con la lotta di classe; il rapporto donna-violenza, riscoperto e riattualizzato; il legame e le differenze con la pratica politica del movimento delle donne. Che questi gruppi siano venuti “dopo” la scoperta del femminismo e la nascita del movimento delle donne, non è solo una questione di tempo storico. Cosi pure il fatto che ne siano collegati,ma anche diversi.

Facciamo parlare per un attimo ancora un loro documento:

Io sono conservazione, autoconservazione, vita quotidiana, adattamento, mediazione dei conflitti, assopimento delle tensioni, sopravvivenza dei miei oggetti d’amore, nutrimento cibo; io sono tutto questo contro me stessa , contro la possibilità di capire chi sono e di costruirmi la mia vita , io sono nella mia pazzia, nella mia autodistruzione. Allora mi guardo dentro e cerco di smettere di pensare ciò che è bene e ciò che è male, al giusto o sbagliato… Sento il bisogno di rompermi, di spaccarmi, di non pensarmi sempre in continuo con la mia storia. Forse perché la mia storia non ce l’ho, forse perché tutto quello che mi viene davanti agli occhi come storia mi sembra qualcosa di altri, mi sembra un abito che mi è stato messo addosso e di cui faccio fatica a svestirmi… Allora cominci a pensare come rompermi, spaccarmi, sezionarmi, riscoprirmi, ricercarmi nella nostra ricerca collettiva nella nostra possibilità, utopia collettiva, vuol dire esprimere spaccatura, rottura contro il potere… vuol dire che non posso rompere con la mia rassegnazione e subordinazione se insieme non rompo contro i nemici che ho individuato, se non riconosco e tiro fuori la mia rabbia, la mia violenza contro l’ideologia e l’apparato di violenza che mi opprime… se non ritrovo con le altre la mia voglia di uscire,di attaccare, di distruggere… Ecco distruggere abbattere tutti i muri le barriere…

Con queste parole si dichiara dunque guerra all’immagine della donna eternamente passiva, marginale, inesistente. Questo infatti, si sostiene, è solo il vestito che il potere ci dà e di cui occorre liberarsi. Non è vero che ogni azione, lotta o atto di ribellione contiene lo spettro del “maschile”. E’ vero invece che ogni cambiamento nasce da una rottura in qualche modo violenta, di fronte alla quale ogni donna prova un senso di angoscia e di paura del vuoto che occorre superare, ma anche un piacere liberatorio determinato dall’aprirsi di nuove prospettive.

Una donna con cui abbiamo parlato, riferiva un interessante elemento sul rapporto tra donna e violenza: non è vero, diceva, che la violenza è estranea alla donna, sostenerlo è riduttivo e sbagliato, poiché violare con un gesto significa tradurre nell’azione l’emotività e dunque sconvolgere la razionalità e la separatezza della politica tradizionale.

In tutti gli scritti in cui si parla di questo problema si usano le parole “forza e fantasia”, “rabbia e creatività” (“l’esterno, il maschile, il nemico, la logica della sopravvivenza e della conservazione, si legge in un documento, hanno sempre contrapposto realtà e fantasia, emozioni e intelligenza, intuito e ragione; per riappropriarci della nostra identità, occorre distruggere questa catena di separatezze”).

La donna dunque, seconda questa ipotesi, è e può essere violenta, il problema è quello di riappropriarsi in modo cosciente di questa sua capacità di ribellione. Finora essa ha praticato la violenza in modo principalmente incosciente e autodistruttivo, non contro l’apparato che la opprime, ma contro se stessa o i suoi figli: il gesto della donna che picchia il bambino è sterile e impotente, cosi come l’abitudine silenziosa a mangiare quotidianamente la propria rabbia. Si tratta ora di dare un nuovo segno a questa violenza istintiva, soffocata o masochista, di darle una possibilità costruttiva e consapevole, di farla diventare una pratica una ricerca, una parte della propria identità e della propria vita. Ma tutto questo può apparire astratto: cosa vuol dire violenza e se significa violare regole imposte, con quali mezzi in quali condizioni e con quali forme.

Per capire meglio occorre azzardare (certamente con molti rischi) non certo una storia (che nessuno vuole raccontare a nome delle altre), ma alcuni elementi di esperienza personale e collettiva che possono avere determinato questa frattura o differenza tra questi gruppi e altri.

All’interno del movimento delle donne l’attenzione è stata posta sulla materialità della contraddizione tra i sessi, sulla sessualità e sulla condizione affettiva, sulle radici profonde che questa contraddizione lascia dentro ognuna di noi. Per cui ogni trasformazione o modificazione non poteva partire che dall’analisi e dalla ricerca di sé. Il rapporto tra le donne, cercato e costruito al di fuori dell’occhio dell’uomo è stato (ed è rimasto) l’unica possibilità reale, sia per la ricerca della propria identità finora nascosta e schiacciata dalla presenza maschile (unico valore riconosciuto), sia per riuscire a spostare “lentamente e faticosamente” i propri rapporti con l’uomo e il mondo dell’uomo (quella che viene definita società).

Del resto il movimento delle donne ha sempre rifiutato e negato qualsiasi possibilità o legittimità di un programma politico generale sconvolgendo in questo modo tutti i parametri della politica tradizionale. Cosi come il “fare” della pratica politica delle donne è stato un “fare” che si è situato fuori da tutti i canali, i riconoscimenti e le misure quantitative, ribaltando le regole di quello che normalmente si intende per prodotto politico.

Questo non ci siano stati prodotti, fatti o luoghi concreti, anche materiali e tangibili come le librerie, i gruppi di self-help o di medicina, o che siano mancate le battaglie, così come la produzione culturale, i luoghi e le case delle donne. Ma la pratica delle donne si è basata principalmente sulla modificazione dei rapporti e sulla ricerca dell’identità personale, sessuale e sociale delle donne, e questa trasformazione è certamente fuori delle regole produttive.

E’ modificazione che sconta i tempi lunghi, i silenzi, le difficoltà dell’apparente vuoto, come pure la mancanza di scadenze e di obiettivi “coprenti”, l’inazione, la paura dell’assenza dell’uomo (con tutti i valori di attivismo che racchiude e rappresenta).

E sconta anche la storia di tante donne perse nei meandri di se stesse, smarrite nelle eterne implicazioni della condizione affettiva, prima analizzata poi continuamente ripetuta come unica dimensione esistenziale (quante donne del movimento ne sono rimaste intricate, quante hanno fatto ritorno alla precedente condizione di emancipazione o di emarginazione, per non essere riuscite a risolverla?). Le giovani leve del movimento hanno senza dubbio guardato con molta distanza e anche con una certa impazienza a questa esperienza cosi intricata e radicale, ma anche lenta ed evanescente.

“Ma insomma dove cazzo è la pratica politica delle donne?” ci ha detto una volta una ragazza reduce da una delle assemblee cittadine sempre affollate, ma in apparenza rissosa ed inconcludente. “Il femminismo mi è servito per prendere coscienza, mi ha aiutato a capire, ma poi?”

Questo “fare” delle donne, questo “lavoro che non finisce mai” perché smaschera le implicazioni profonde e la forza del potere dentro la vita personale e la vita politica, ha potuto sembrare ghettizzato, senza un rapporto diretto, un confronto o uno scontro con il potere e le istituzioni. Per contrasto certamente, o forse per richiamo o necessità, è nata la voglia di rivendicare un’azione più diretta e visibile e di sperimentare anche una pratica attiva di violenza. E’ forse allora per questa strada che è possibile rintracciare la base, la realtà politica e anche le motivazioni e il significato della scelta della “nuova” violenza delle donne. Quali sono poi state le caratteristiche precise e gli sbocchi concreti di questa posizione?

Come primo dato si registra un maggiore legame con la lotta di classe comunemente intesa, nelle forme storiche delle sue battaglie e nello scontro diretto con le istituzioni. Si combatte, come dimostrano gli obiettivi delle azioni rivendicate dai volantini, lo sfruttamento del lavoro femminile, nero o delle detenute; l’ordine dei medici, ossia la struttura che sintetizza in sé la violenza sulla donna attuata dal capitale e dall’uomo; si cerca il rapporto con le donne carcerate. Le donne in carcere rappresentano infatti l’antagonismo più radicale tra la donna e le istituzioni, e la loro presenza nelle lotte deve trovare una precisa corrispondenza nelle lotte delle altre donne.

“Il movimento femminista deve farsi carico della lotta delle detenute”, si legge in uno scritto, “proprio per rompere l’isolamento sociale che una detenuta vive nelle mura del carcere”. E lo slogan: “streghe fuori, streghe dentro, siamo tutte nel movimento”, lo conferma.

Ma gli obiettivi non si limitano a questo: più in generale si proclama la ribellione contro tutto ciò che opprime le donne. “La violenza carnale”, si legge in un altro documento, “è uno dei modi con cui il sistema capitalistico patriarcale esercita il potere su di noi… terrorismo maschile e terrorismo di stato collaborano insieme contro di noi, le donne, per salvare il potere dello stato e i miserabili privilegi del maschio “.

Pertanto maggiore interesse e intervento di lotta nei confronti delle istituzioni si potrebbe pensare; tuttavia questi gruppi di donne non mantengono maggiori rapporti con le organizzazioni maschili della lotta di classe. Rivendicano invece una netta autonomia e una gestione delle proprie forme di organizzazione rigidamente separate. E rispetto alla violenza politica, cosi come viene oggi definita dai teorici dello scontro di classe in Italia, tendono a precisare il significato e i termini di una partecipazione autonoma e specifica delle donne. “Anche nella rivoluzione socialista, ha detto una compagna, se non arriveremo con una capacità e una forza autonoma, ci troveremo di fronte a una struttura repressiva”.

Il secondo dato, proprio a partire da questa ricerca di autonomia, riguarda l’analisi che questi gruppi fanno del tipo e del livello di violenza che le donne sono in grado di praticare.

Stabilito che l’uomo non è “violenza” e la donna “dolcezza” (perché questa divisione è stata operata dagli uomini contro le donne) e che la violenza non è né maschile né femminile, ma se mai la differenza è tra violenza liberata e non liberata, si tratta allora di provare a viverla e a praticarla in modo diverso. Evitando che essa produca, seguendo regole proprie e inglobanti, quella che viene definita la “militarizzazione delle coscienze”.

Ricondotta in questo modo all’espressione del soggetto la violenza non può essere definita con regole determinate e prefissate, sovrapposte o imposte ai bisogni individuali. Non può essere una scienza, una strategia, ricondotta solamente alle analisi generali o alle leggi della guerra, ma deve rimanere una parte della liberazione personale, un gesto o un comportamento che fa parte dell’esperienza e della ritrovata espressione di sé, in cui la donna deve potersi riconoscere tutta intera. “L’importante è”, diceva ancora una compagna, “affrontare la realtà con una buona dose di azione e di volontà”

Resta dunque una valenza politica “aperta”, una specie di insubordinazione intima e profonda. E’ questo infatti il dato caratteristico di queste analisi: volersi ribellare alle situazioni di passività, senza volere teorizzare la violenza a tutti i costi. E se la situazione che si vuole mutare riguarda l’uomo o la famiglia, l’azione acquisterà un segno e una forma precisa. Sono infatti i nuovi rapporti tra donne, o forse una comune, a riuscire a trasformare la gabbia della famiglia. Ma se l’oppressione subita proviene da un’istituzione come l’ordine dei medici o le strutture ospedaliere, allora, a giudizio di questi gruppi, si richiede l’esercizio di una ribellione collettiva e organizzata, uno scontro diretto, senza paura della violenza. Possono andare benissimo un incendio o una molotov.

L’ultimo dato che risulta chiaro a questo punto è la differenza tra la “nuova” violenza praticata in queste forme saltuarie, specificatamente da gruppi di sole donne, e quella dei gruppi armati (Nap, Br e altri) non sfiorati dalla problematica del femminismo. Pur non facendo aperta condanna politica, le donne di quei gruppi in parte riconoscono questa differenza e si pongono molte domande.

La donna si sostiene, per sua natura e per sua storia, fuori del potere e dello stato, è sempre stata disponibile alle scelte più radicali, anche se per la sua condizione di “reclusa” della famiglia ha potuto parteciparvi solo come “consistente minoranza”. Quando però ha scelto questa strada si è scontrata con un modello elitario ed estraneo, non suo, non fatto per lei. All’interno dei gruppi clandestini la donna può vivere certamente il massimo della parificazione con l’uomo, fino all’assunzione del potere delle armi, può assumere posizioni di rilievo, guidare azioni, vivere il gioco liberatorio e l’avventura dell’essere fuori e contro tutte le istituzioni.

Ma quanta parte del suo essere donna si esprime, quanto deve accantonare la contraddizione specifica del suo sesso, e anche le possibilità di gestire e di vivere una sua forma di ribellione e di violenza? Quanta costrizione, rigidità disciplina determinata dalla gestione maschile della lotta deve subire?

Molte fra queste donne, intendono rivalutare la violenza in termini liberatori, come “forza ritrovata”, e pongono il problema della mancanza di corrispondenza tra questo tipo di esperienza cosciente e quella maschile, tradizionale, classica che i gruppi armati esprimono cosi bene. Del resto, ci ha fatto notare una compagna in un gruppo che pratica la lotta armata, per forza di cose è massima la mediazione tra le esigenze personali, la ricchezza e le potenzialità di un individuo e il gesto che si vuole compiere, quantificato e simbolico, che finisce per ridurre, nella sua rappresentazione, gran parte della persona umana. Non è possibile infatti in queste condizioni discutere ogni azione mentre la si compie, la si decide, la si prepara, e tanto meno gli effetti e le modificazioni che produce su chi la pratica. Non è possibile dunque gestirla fino in fondo, renderla parte della propria liberazione cosciente.

Ma sul problema della violenza, riscoperta o rifiutata, individuale o collettiva, ricomposta alle esigenze di liberazione personale o ad esse sovrapposta, si riaprono i mille perché della politica delle donne.

Dietro l’apparente attivismo sociale alcune scorgono l’immobilità della donna, la difficoltà di uscire dalla propria condizione mimetica che ripete, magari inconsapevolmente i modi e le forme delle lotte dell’uomo. Altre pensano che questo tipo di azione “diretta” abbia il significato di buttare nel calderone del ribellismo generale, un’ansia di liberazione non ancora approfondita. E che tutto questo copra (anche con tutti gli effetti positivi che comporta scaricare una giusta rabbia) l’incapacità di costruire una forza e una identità reale ed autonoma, fuori dalle regole dello scontro.

Molte donne, poi, si rifiutano di vedere una contrapposizione tra lotta personale e lotta sociale, perché qualsiasi costruzione personale e collettiva della propria identità, tra donne, è già lotta e contiene una buona parte di violenza liberata.

Del resto quelle che sono le scoperte delle donne su di sé in un processo collettivo di riconoscimento è storia di oggi, non descrivibile in modo definitivo e, speriamo, non è chiusa e strozzata dalla fretta di ottenere dei risultati quantificati e riconosciuti che risolvano una volta per tutte, tutti i problemi.

Poiché il disagio delle donne proviene da una storia di lungo silenzio e la loro parola si deve districare tra tutte le parole degli uomini.
