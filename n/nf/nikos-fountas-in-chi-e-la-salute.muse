#title In chi è la salute?
#subtitle Contributi ad un dibattito interno all’anarchismo italiano
#author Nikos Fountas
#LISTtitle In chi è la salute?
#SORTauthors Fountas, Nikos;
#SORTtopics rivendicazione, azione diretta, FAI/FRI, polizia, critica, insurrezionalismo
#date dicembre 2017
#source Consultato il 20 febbraio 2018 su [[https://perifrourisisocialanarchism.noblogs.org/post/2017/12/23/in-chi-e-la-salute-contributi-ad-un-dibattito-interno-allanarchismo-italiana-1/][perifrourisisocialanarchism.noblogs.org]]
#lang it
#pubdate 2018-02-21T23:25:01
#notes Scritto in risposta a *Disfattismo pirotecnico*<br>
Pubblicato in contemporaneo a un altro testo, *Davanti alla rivoluzione*



*Dopo la rivendicazione dell’attacco alla caserma S. Giovanni è seguita la risposta di Umanità Nova. Come anarchici slegati da aree o gruppi ci sentiamo di contribuire al dibattito con due scritti separati ma con la comune volontà di suggerire un approccio differente a un dibattito che si sta trascinando da troppi anni. Questo è il primo dei due contributi inviati a Umanità Nova.*

“La Salute è in voi!”, recitava un opuscolo uscito nel 1906 con il settimanale anarchico italoamericano “Cronaca Sovversiva”.

Si trattava fondamentalmente di un manuale pratico per sabotaggi, fabbricazione di esplosivi e guerriglia che, seppur oggi decisamente datato a fronte della pubblicistica anche solo degli anni ’70 (si pensi agli scritti della Rote Armee Fraktion o di Carlos Marighella), rappresenta un dato storico fondamentale per capire cosa fosse l’anarchismo di lingua italiana.

Facciamo un breve passo avanti: nel 1927 vengono giustiziati Sacco e Vanzetti, accusati di rapina e omicidio al calzaturificio «Slater and Morrill» di South Braintree.

Giustiziati perchè anarchici e immigrati, innocenti per quello specifico atto.

Si trattava però non di due “piccoli angeli”, come per molti anni li ha dipinti la vulgata anche anarchica, ma di due combattenti rivoluzionari avvezzi al far esplodere le case dei questori e all’uso delle armi[1] tanto quanto alla propaganda di massa, all’agitazione sindacale e allo sciopero.

Un’altro salto in avanti: nell’articolo “Disfattismo pirotecnico” di T. A., pubblicato su Umanità Nova il 17 dicembre[2] si prende in esame l’attacco ad una caserma romana dei carabinieri avvenuto fra il 6 e il 7 dicembre, facendo una serie di affermazioni che mettono insieme di tutto e un po’, in una spergiura pressochè totale dell’evento, delle persone idealmente coinvolte e di tutti i significati che vi sono legati.

Per capirci, chi scrive non è un sostenitore dell’insurrezionalismo inteso come impalcatura ideologica poichè ritiene l’ “insurrezione” come solo uno dei tanti passaggi in cui un percorso rivoluzionario si può snodare e che caricarlo di significati eccessivi è quantomeno miope, anche perchè la realtà attuale ci dimostra semmai che la “Guerra sociale[3]” non è materia di insurrezioni, ma di lente e altamente complesse guerre di logoramento che nei momenti più conflittuali diventano guerre di trincea[4].

Insomma la risibilità dell’opzione insurrezionale non significa che scompaia il dato del conflitto, né quello militare, ma che passano dall’essere una “gioia” ad essere qualcosa di estremamente serio e sporco[5].

E’ centrale quindi distinguere fra Insurrezionalismi e pratiche insurrezionali.

Questa parentesi vuole essere introduttiva ad un altro concetto: se dobbiamo criticare gli insurrezionalismi bisogna farlo innanzitutto da un dato politico, quindi da un dato pratico (ovvero proponendo alternative credibili) ma soprattutto sempre mantenendo il rispetto per chi lotta anche in modo estremamente diverso dal nostro.

L’articolo del compagno su Umanità Nova pone tre critiche fondamentali:

Innanzitutto si afferma che azioni di guerriglia urbana sono per loro stessa natura settarie, aristocratiche e opposte ad un lavoro sociale e che comportano il rifiuto di un qualsiasi confronto con chi non si pone sullo stesso piano.

Lo scritto centra molti punti: innanzitutto che la chiusura in sé stessi e la mancanza di un confronto con quelle soggettività non così radicali è un freno per la penetrazione nel tessuto sociale, così come invece è fondamentale immergersi nelle masse, avviarvi un rapporto simbiotico e di progressiva infezione.

Ma si tratta di questioni diverse.

Innanzitutto l’azione di guerriglia non è per forza alienante rispetto ad un lavoro di massa: la consistente simpatia di cui godettero formazioni lottarmatiste in Italia e Germania Ovest, per non parlare di quella che perdura tutt’ora in Grecia, sono una dimostrazione del contrario. Allo stesso modo, le riflessioni della prima Prima Linea[6] sul ruolo del militante pubblico e del militante armato, così come quelle dell’Autonomia organizzata sul “doppio livello”, posso dare spunti importanti.

Semmai si può criticare l’opportunità e le modalità di uno specifico atto, la sua convenienza e il suo svolgimento. Infatti, per esempio, se la lotta armata non è assolutamente un’opzione nel nostro contesto, il discorso del doppio livello si può applicare anche a chi partecipa a pratiche insurrezionali di altro tipo, quali il Blocco Nero e gli scontri di piazza.

Ma anche qui, al netto di tutto, va però mantenuto l’adeguato rispetto per chi si pone in rischio per proseguire una lotta: no, nessuno ha piazzato una bomba davanti ad una caserma dei carabinieri apposta per fare un dispetto ad altri anarchici, anche perchè non serve certo l’insurrezionalismo per mandare in crisi l’anarchismo comunista/sociale in Italia: lo dimostrano i numeri scarsi, l’irrilevanza politica, i circoli che chiudono, il ricambio che non c’è, i congressi sempre più deserti. Ci si può raccontare che va tutto bene, ma farlo non lo rende vero.

E no, non bisogna dare acqua al mulino degli scazzi, provando semmai semmai ricucire nel possibile i rapporti personali, amicali e politici con quei/lle militanti che dimostrano di agire in buona fede, ponendo domande, critiche e risposte alternative, e non controscomuniche.

Quindi, su “Disfattismo pirotecnico”, si afferma che il clima di “pace sociale” in Italia non è così reale poichè ci sarebbe un fiorire di mobilitazioni sempre più forti e radicali.

Si torna all’uomo che cade da un palazzo di cinquanta piani e che per farsi coraggio si ripete: “Fino a qui, tutto bene. Fino a qui, tutto bene”.

La verità è che la riscrittura genetica della società sta andando avanti spedita: i centri città sono completamente trasformati e ora i processi di gentrificazione e normalizzazione si stanno allargando ai quartieri della prima periferia. Gli sgomberi e i sequestri hanno avuto un ritmo terrificante negli ultimi due anni, arrivando ad annichilire intere aree politiche proprio mentre il recupero riformista riusciva ad inglobare numerosi settori di movimento. Le destre, anche quelle più neonaziste quali Lealtà&Azione e il VFS, hanno ormai tutte un respiro nazionale forti di una lento ma progressivo radicamento nelle classi popolari.

La debolezza del cosiddetto “movimento” è provabile numericamente guardando due appuntamenti “nazionali” a meno di quattro anni di distanza: se nel 15 ottobre 2011 si portarono in piazza 300.000 persone, nel NoExpo del 2015 ve ne erano 50.000.

Si può tirare in ballo la solita storia degli “scontri che oscurano il corteo” a livello mediatico, ma in verità se il corteo avesse sfilato pacifico, ci sarebbe stato giusto giusto un articoletto distratto, per plaudere la pacificità dei manifestanti e la bravura della polizia.

Un problema centrale emerge quasi involontariamente da un passaggio secondario dell’articolo di T.A.:

<quote>

“<em>La via di uscita è […] imparare dalle masse quali sono i temi che le toccano maggiormente e, coerentemente con il nostro ideale e la nostra prassi, possano dar vita a movimenti di ribellione, imparare dalle masse quali sono gli organismi che possono divenire strumenti di autorganizzazione e di azione diretta, imparare dalle masse quali sono i linguaggi che possano rendere più accessibile la propaganda anarchica.</em>“

</quote>

Insomma ci si pone sempre e comunque come soggetti che (a discapito del nome di “minoranza agente”) paiono quasi cadere dal pero nei rapporti con la popolazione, subendoli e non essendovi attivi.

Qualcosa di completamente diverso da ciò che andrebbe fatto, poiché l’anarchismo è stato grande quando è stato politico, quando nella sua storia ha preso in mano le redini del discorso e ha inziato percorsi di largo respiro e forte intensità, razionalmente e strategicamente pensati, all’interno e in simbiosi con la popolazione, e non come soggetti “a servizio” della stessa, partendo dalla logica perdente dell’autodissoluzione qual’ora le cose andassero bene.

Si tratta semmai di porsi come i germi delle istituzioni del “mondo nuovo” che portiamo nei nostri cuori, come Murray Bookchin (ideologo a monte dell’unica rivoluzione contemporanea) giustamente esponeva: le assemblee di movimento devono diventare le assemblee di gestione della comune, i servizi d’ordine le sue milizie e i gruppi politici i suoi organi esecutivi.

Questo stesso percorso è stato alla base del movimento anarchico di maggior successo oggi, quello greco, che ha scelto durante il 2008 di spargersi sul territorio e di diventare il punto di riferimento delle proprie comunità, partecipandole a livello di sicurezza, di sanità, di lotte lavorative, e molto altro.

Esemplare è l’attività del K*Box, caffetteria occupata ad Exarchia, che oltre ad essere un luogo di socialità ha aperto l’ambulatorio autogestito, sostiene occupazioni abitative, combatte attivamente contro le narcomafie in quartiere e il cui collettivo politico, Rouvikonas si è reso noto per molteplici azioni contro problemi concreti della società greca quali la corruzione delle autorità ospedaliere, le agenzie di “liste nere” per i morosi nei confronti delle banche o le missioni di aiuto nelle zone alluvionate di Madra.

E’ così che si può porre una critica all’insurrezionalismo: da pari. Da pari portando rispetto verso chi lotta e viene represso (e potendo così rivendicare per sé lo stesso tipo di rispetto), da pari proponendo un anarchismo competitivo ed efficace, contemporaneo e accattivante, che sia realmente quello che vuole essere.

Si tratta di fare la cosa giusta indipendentemente che venga fatta dall’altra parte, perchè se siamo anarchici e anarchiche lottiamo per ciò che è giusto.

<quote>

“[visto che] <em>Sembra il momento di far uscire una qualche tipo di conclusione, dirò che – suppongo – non credo che le strutture o le forme di associazione volontaria che adottiamo controllino deterministicamente i nostri risultati (pur avendo una forte influenza, come la hanno tutti gli strumenti, su chi le adotta), ma che tutte le strutture e le strategie sviluppate fino ad oggi dagli anarchici abbiano serie debolezze e che queste carenze saranno letali, a meno che non si sia più onesti, flessibili, ricettivi alle critiche ed energici di quanto si sia stati finora.</em>[7]”

</quote>

                                       <em>Nikos Fountas</em>

[1] Per una storia più completa, il fondamentale libro di Paul Avrich, <em>Ribelli In Paradiso. Sacco, Vanzetti e il movimento anarchico negli Stati Uniti</em>, 2015, Nuova Delfi Libri, Roma, traduzione a cura di Antonio Senta.

[2] [[http://www.umanitanova.org/2017/12/17/disfattismo-pirotecnico/][www.umanitanova.org]]

[3] Termine molto usato da uno dei più interessanti autori anarchici dei giorni nostri, lo statunitense Peter Gelderloos, che con esso indica una condizione di vero e proprio conflitto bellico fra oppressi e oppressori, con il dispiegamento di strategie (la controinsorgenza), risorse (militarizzazione dei territori e delle forze di polizia) e legittimazioni (diritto penale del nemico, leggi anti-terrorismo, cultura del “degrado” come pericolosità…) militari.

[4] Si pensi al ruolo che hanno assunto i luoghi nei conflitti sociali contemporanei, dalla difesa di territori rurali (Zad, Bure, Hambach, Val Susa) e metropolitani (Exarchia, Gezi park, piazza Maidan) alla lotta per eradicare, mantenere o guadagnare edifici fisici (sedi fasciste, centri sociali, occupazioni). Da notare che in Italia si è sempre affrontato il problema da un punto di vista etico-filosofico e mai da un punto di vista strategio e pratico, come per esempio sulla necessita di mantenere strutture logistiche e basi sicure.

[5] Già Nestor Makhno lo notava quando scriveva: “In una rivoluzione sociale il momento più critico non è il momento del crollo del Potere, ma il momento immediatamente successivo, il momento in cui quanti sono stati spodestati attaccheranno i lavoratori e questi ultimi dovranno difendere le conquiste appena realizzate” poichè “la classe dominante conserverà a lungo una grande capacità di resistenza e per molti anni sarà in grado di sferrare attacchi contro la rivoluzione cercando di riconquistare il potere e i privilegi che le sono stati sottratti” (in Delo Truda, n16 p. 5-4, riportato in Alexander Shubin, <em>Nestor Machno: bandiera nera sull’Ucraina. Guerriglia libertaria e rivoluzione contadina (1917-1921)</em>, p. 190-191, Elèuthera, 2012). Stessa cosa di cui parlerà cinquant’anni dopo Adriano Sofri su Giovane Critica, n19, 1968-1969, seppur in termini operaisti. Insomma, l’operaismo e l’autonomia riprendono concetti anarchici decenni dopo e, al contrario nostro, riescono a tenerli a mente.

[6] Si spera che si riesca a discutere con il necessario distacco di temi come P.L., analizzandoli da un punto di vista storico e politico, senza inutili emotività.

[7] Peter Gelderloos, “<em>Insurrection vs. Organization. Reflession on a pointless schism.</em>“, 2007, recuperabile a [[http://theanarchistlibrary.org/library/peter-gelderloos-insurrection-vs-organization][theanarchistlibrary.org]]



