#title Nel paese delle democrazie
#LISTtitle Nel paese delle democrazie
#author Finimondo
#SORTauthors Finimondo
#SORTtopics repressione
#date 2015
#source Consultato il 30 novembre 2017 su [[http://tabularasa.anarhija.net/library/alice-it][tabularasa.anarhija.net]]
#lang it
#pubdate 2017-11-30T22:00:00

<quote>
«La questione — disse Alice — è sapere se hai il potere
di attribuire alle parole tanti significati diversi.
La questione — disse Humpty Dumpty —
è sapere chi comanda... tutto qui»
</quote>

Alice, idealista un po’ ingenua, si sta chiedendo in questi giorni se è mai possibile che la parola «terrorismo» abbia un altro significato, dizionario storico-etimologico alla mano. Humpty Dumpty, materialista un po’ grezzo, le risponde che essendo lo Stato a comandare, ed essendo il linguaggio proprietà di chi comanda, allora «terrorismo» significa ciò che vuole lo Stato. Tutto qui.

Negli anni 70 lo Stato concedeva l’appellativo di «terrorista» a chiunque gli contendesse il monopolio dell’uso della violenza, ovvero utilizzasse armi da fuoco o esplosivi, soprattutto ai partecipanti di organizzazioni specifiche combattenti, soprattutto se quelle organizzazioni erano espressione di un più vasto movimento di contestazione, soprattutto se quella contestazione mirava a sfociare in una rivoluzione. Per lo Stato, «terrorista» era soprattutto chi lo attaccava a mano armata.

Oggi che le organizzazioni armate specifiche sono quasi del tutto scomparse, che gli arsenali sovversivi sono desolatamente vuoti, che raramente i movimenti di contestazione assumono dimensioni considerevoli, che (quasi) mai pongono la questione rivoluzionaria, Alice vorrebbe dedurne che lo Stato abbia rinunciato all’uso di questo termine, reputandolo incomprensibile se non in sporadici casi. Già le era insopportabile la definizione di «terrorista» rivolta a chi prendeva di mira gendarmi e magistrati, piuttosto che a chi faceva strage di pendolari e passanti, ma insomma... sapete com’è la gente, quando vede spargimento di sangue si impaurisce e si confonde. Bisogna supporre che per la propaganda non sia stato poi troppo difficile far cadere le persone nell’equivoco, demonizzare il regicida e non il tiranno. Ma ora, via, dopo che negli ultimi decenni si è assistito ad un così triste calo di funerali istituzionali, facciamola finita con lo spauracchio del «terrorismo»!

Invece no. In questa epoca così sprovvista di «nemici esterni» credibili, ma al tempo stesso carente di solidi consensi, quando attorno non è rimasto più nessuno ad applaudirlo, lo Stato ha pensato bene di giocare d’anticipo, di non attendere la comparsa di qualche minaccia sovversiva per poter dispiegare la macchina da guerra della retorica anti-terrorista: meglio prevenire che reprimere. Ma prevenire <em>chi</em> dal fare <em>cosa</em>? Come sosteneva un profondo conoscitore dell’arte di governare, «Mentre gli individui tendono, sospinti dai loro egoismi, all’atonismo sociale, lo Stato rappresenta una organizzazione e una limitazione. L’individuo tende a evadere continuamente. Tende a disubbidire alle leggi, a non pagare i tributi, a non fare la guerra. Pochi sono coloro — eroi o santi — che sacrificano il proprio io sull’altare dello Stato. Tutti gli altri sono in stato di rivolta potenziale contro lo Stato».

Sarà per questo che lo Stato si è preso la licenza di definire «terrorista» chiunque lo critica, lo avversa, lo contrasta, senza fare troppe distinzioni fra il significato delle parole e la natura dei fatti? Per il fatto che, a parte i santi da pregare e gli eroi da decorare, tutti gli altri sarebbero potenziali ribelli?

«Ma ciò non ha senso!». Certo che no, dolce Alice, però tieni sempre a mente <em>chi</em> comanda. Sta tutto lì.

*** Com’è cominciata 

Il 2001 sarà un anno difficile da dimenticare, per la svolta che ha segnato. Non foss’altro perché gli eventi di quella estate, nell’arco di poche settimane, hanno contribuito a cambiare la vita quotidiana di milioni di persone. Dopo le torride giornate di fine luglio a Genova, quando le manifestazioni contro il periodico vertice dei Grandi della Terra sono state insanguinate da una mattanza generalizzata, si è avuto un secondo martedì di settembre che ha visto il più incredibile attentato contro luoghi del dominio economico e militare mai avvenuto sul suolo degli Stati Uniti. Criticati teoricamente e praticamente da movimenti sociali e radicali da una parte, attaccati militarmente da gruppi integralisti dall’altra, non è sorprendente che i governi occidentali nel loro insieme abbiano deciso di mettere mano ai loro formalismi legali, modificandoli per poter neutralizzare più facilmente i loro contestatori.

Dalla fine del 2001 ad oggi abbiamo assistito in molti paesi ad un incremento delle leggi repressive, ad una vera e propria ristrutturazione dell’ambito del diritto in grado di garantire, se non la pace nei mercati, almeno la quiete nelle strade. Se la minaccia del «terrorismo» rimane lo spauracchio prediletto con cui giustificare un controllo sempre più pervasivo della vita sociale, nonché una incessante limitazione della libertà individuale, non costituisce comunque il solo strumento. Quale che sia la motivazione adottata per la loro attuazione, appare chiaro che i vari provvedimenti legislativi tendono non solo a conformarsi ad un’unica direttiva generale, ma anche a venire applicati in ogni campo ritenuto sensibile. Il blocco amministrativo dei siti, ad esempio, è iniziato con la battaglia contro la «pedo-pornografia», ma verrà presto esteso dappertutto a quella «anti-terrorismo». A loro volta, le specifiche misure di sicurezza prese non si limitano a prevenire un genere di reati (il recente acquisto da parte delle ferrovie francesi di numerosi droni da dislocare lungo i binari sarà pure mirato a scongiurare eventuali furti di cavi di rame, come ufficialmente dichiarato, ma tornerà senz’altro utile anche nella prevenzione dei sabotaggi). E così via di seguito.

Ora, per quanto interessante, non ha qui molto senso fare un inventario delle varie misure prese in Europa nell’ambito della lotta contro il terrorismo. Anche perché, dall’incriminazione di alcuni sindacalisti di base italiani rei di non aver tentato di fermare i black bloc nel 2001, passando per la messa sotto accusa di un paio di adolescenti inglesi trovati in possesso di libri di ricette anarchiche nel 2007, fino ad arrivare alla condanna a 7 anni di detenzione di un islamico francese rientrato da un breve soggiorno in Siria nel 2014 — tanto per fare solo alcuni esempi — rischieremmo di perderci in un labirinto il cui scopo sarebbe quello di spingerci tutti verso un’unica uscita: un’ineluttabile obbedienza cieca e passiva. Ci sembra più interessante sforzarci di comprendere cosa frulla nella testa dei difensori dell’ordine sociale, di leggerne in controluce le preoccupazioni maggiori.

*** Lo Stato si attrezza

Innanzitutto, bisognerebbe riuscire a togliersi dalla testa una idea tanto facile quanto comoda. Che quanto sta accadendo in termini di privazione di libertà sia senza prece- denti, che siamo di fronte ad una inaudita criminalizzazione esplicitamente indirizzata ai movimenti di lotta. Non è vero. Siamo di fronte alla comune prassi di qualsiasi governo, rivolta contro chiunque, e mirante alla normalizzazione forzata della vita, alla sua codificazione istituzionale, alla sua standardizzazione tecnologica. Il libertario non può e non deve essere più libero di protestare, così come il libertino non può e non deve essere più libero di eccitarsi: protesta ed eccitazione devono essere controllate, non devono uscire da schemi prestabiliti. Sono i moderni strumenti tecnologici di cui dispone oggi lo Stato a rendere questa imposizione possibile, e quindi pensabile, e perciò perseguibile, ed infine capillare. Ed è solo la nostra memoria a corto raggio a renderla stupefacente ed impressionante. Niente di più, niente di meno.

Il “Patriot Act” e le varie Guantanamo in giro per il mondo non possono meravigliare se si pensa che quando gli Stati Uniti scesero in guerra contro la Germania, nel lontano aprile del 1917, si aprì uno dei periodi più bui della storia statunitense, dove <em>chiunque</em> non mostrasse un incondizionato fervore patriottico veniva braccato e perseguitato. Allora si scatenò una vera caccia alle streghe che ebbe il suo apice fra il novembre 1919 ed il febbraio 1920, un periodo in cui la polizia invadeva le abitazioni di migliaia di persone, arrestando chiunque vi si trovasse all’interno. Le retate erano condotte senza seguire minimamente i termini di legge. Uomini e donne venivano arrestati senza mandato, picchiati con brutalità per le strade, trascinati e rinchiusi in centri detentivi per settimane e mesi senza avere la possibilità di avvisare parenti o avvocati. Talvolta pure chi si recava a visitare i prigionieri veniva messo dietro alle sbarre, in base alla nota teoria che solo un sovversivo possa preoccuparsi della sorte di un sovversivo. In quei quattro mesi si aprirono le procedure per la deportazione di tremila immigrati. Di questi circa 800, parecchi dei quali anarchici, furono alla fine effettivamente espulsi, sebbene quasi nessuno di loro fosse stato condannato per aver commesso un qualsivoglia reato. Un noto docente di diritto dell’epoca spiegava: «quando stai cercando di proteggere la comunità contro ratti morali, a volte devi pensare più all’efficacia della trappola che alla sua costruzione rispettosa della legge». Ad un secolo di distanza, dove sta la differenza sostanziale? I ratti morali statunitensi si incrociano con la <em>racaille</em> francese, o con le zecche italiane, in una esecrazione perbenista che accomuna in un sol fascio tutti coloro che non s’inginocchiano davanti a questo mondo in cui l’unica libertà permessa è quella del consumo sfrenato di merci. Contro di loro lo Stato è in guerra, non da oggi, ma da sempre. E l’«anti-terrorismo» è indubbiamente una delle sue armi principali, da usare come meglio crede senza porsi troppi problemi di coerenza storica. Per comprendere ciò possiamo anche rimanere qui nella vecchia Europa.

Lo Stato spagnolo, ad esempio, non ha dovuto affatto attendere l’11 settembre 2001 per espandere il concetto più comune di «terrorismo» — quello che richiede la presenza di una organizzazione armata specifica — ad una serie di atti che avvengono durante i disordini sociali. L’articolo 577 del codice penale vigente nel paese delle incarcerazioni «<em>incommunicados</em>», li colpiva già nel 1995. Si va dalle lesioni personali alle minacce, dall’incendio al danneggiamento, e tutto ciò senza dover più appartenere ad una vera e propria banda armata. Basta avere «l’intenzione di sovvertire l’ordine costituzionale o di disturbare seriamente l’ordine pubblico», o di perseguire questi fini spaventando non solo gli abitanti di una comunità urbana, ma persino «i membri di gruppi sociali, politici o professionali». Allo stesso modo la legislatura iberica prevedeva già la punizione della «esaltazione del terrorismo» attraverso una qualsiasi forma di espressione pubblica, ovvero ogni incitazione o «giustificazione» a compiere atti considerati terroristici, nonché ogni applauso a chi se ne renda responsabile. Perseguibili, oltre alle minacce, sono da tempo anche «gli insulti e i seri disturbi del funzionamento delle assemblee locali», provocati da chi manifesta il proprio sostegno a gruppi di «terroristi». Come si vede, si tratta di definizioni talmente elastiche da poter essere applicate a qualsiasi movimento minimamente combattivo. Basterebbe fare irruzione in un consiglio comunale a insultare i politici che stanno votando a favore di un progetto nocivo, o fare dei blocchi stradali per fermarne i lavori, per potervi ricadere dentro. Stiamo parlando dello stesso Stato che nel corso degli anni 80, sotto un regime socialista, quindi di sinistra, ha creato un squadrone della morte che ha commesso 28 omicidi ai danni di presunti militanti dell’ETA.

Non si sta parlando di un piccolo e lontano paese dell’America Latina, ma della grande e vicina Spagna europea, la quale ha da poco varato una «legge sulla sicurezza cittadina» che privilegia una facile sanzione amministrativa rispetto ad una spesso complicata repressione giudiziaria. Le multe saranno divise in tre scaglioni e pioveranno addosso a chiunque “disturbi” la quiete pubblica.

Veniamo ad oggi e diamo pure uno sguardo alla terra dei diritti dell’uomo, alla patria della rivoluzione, alla culla dell’illuminismo: la Francia. Qui il governo sta varando nuove leggi anti-terrorismo che prevedono novità significative. Ad esempio si farà piazza pulita di un ostacolo su cui spesso inciampavano le inchieste, la necessaria presenza di una associazione. Attraverso l’istituzione giuridica della «impresa individuale terrorista», anche i lupi solitari potranno essere rinchiusi in gabbia senza difficoltà (fino a 10 anni di carcere, oltre al pagamento di ammende pari a 150.000 euro). Non dovranno nemmeno essere colti in flagranza di chissà quale reato, giacché basterà che singoli individui possiedano o cerchino potenziali mezzi (come le sostanze per accendere camini o lo zucchero?), cerchino e controllino possibili obiettivi (come passare davanti a banche o a caserme?), leggano siti web sospetti (come quelli di contro-informazione?) per potersi vedere incriminati. I legislatori francesi la chiamano «neutralizzazione giudiziaria preventiva». Quanto agli atti perseguiti, un lupo solitario non dovrà arrivare a sbranare nessuno perché gli basterà causare semplici «degradazioni» per trovarsi braccato come «terrorista». La nuova legislazione francese in materia prevede anche di inasprire le pene contro l’«apologia di terrorismo», sia che questa rimanga circoscritta in spazi privati (3 anni di detenzione e 45.000 euro di multa) sia che avvenga in spazi pubblici (5 anni di detenzione e 75.000 euro di multa). In quest’ultimo caso l’uso di Internet sarà considerato un’aggravante (fino a 7 anni di detenzione e 100.000 euro di multa). Ovviamente è previsto il «blocco amministrativo» dei siti che saluteranno le azioni dirette o sosterranno certe lotte, in quanto colpevoli di «apologia di terrorismo».

Misure queste che erano già in discussione prima del massacro della redazione di Charlie Hebdo, dopo il quale non potranno che peggiorare. Sull’onda dell’indignazione per l’eccidio, non solo le strade di Parigi si sono riempite di uomini in uniforme a caccia di sospetti, ma si è assistito fra l’indifferenza e l’imbarazzo (con qualche debole protesta) ad una repressione che colpisce la libertà di pensiero e di parola. Non sono pochi coloro che sono stati inquisiti e qualcuno già condannato a pene detentive solo per aver espresso il proprio parere non esattamente di cordoglio per le vittime.

A questo proposito, ricordiamo che lo scorso maggio 32 paesi hanno ratificato il protocollo europeo in materia, quel CECPT il cui articolo 5 definisce l’«apologia di terrorismo» come la diffusione volontaria di «un messaggio al pubblico con l’intento di istigare a commettere un “reato terroristico”, qualora tale comportamento, anche non sostenendo direttamente reati di terrorismo, dia luogo al rischio che uno o più reati terroristici possano essere commessi». Da reprimere è quindi qualsiasi messaggio che, anche <em>senza sostenere direttamente</em> la perpetrazione di reati, <em>dia luogo al rischio</em> che qualcuno <em>possa</em> passare alle vie di fatto contro il potere. Fuori della propaganda più servile, il silenzio o la galera.

Per quanto riguarda invece la pirateria informatica — ovvero l’«accesso fraudolento in un sistema di trattamento automatizzato dati, ostacolo al suo funzionamento e introduzione, soppressione o modificazione fraudolenta dei dati» — nella nuova legislazione francese sarà prevista l’aggravante di «banda organizzata». Ciò significa che, ad esempio, gli attivisti di Anonymous andranno incontro ad una condanna a 10 anni di carcere e a 1.000.000 di euro di multa. Inoltre, progettato per ostacolare la circolazione oltre il confine di aspiranti martiri della guerra santa, ma applicabile anche ai sostenitori della guerra sociale, verrà istituito un divieto amministrativo di uscita dal paese per tutte le teste calde già schedate, con ritiro di passaporto e carta di identità, per un periodo di 6 mesi rinnovabile all’infinito.

E poiché le norme in materia tendono a venir omologate a livello europeo, è facile supporre che non ci vorrà molto prima di sentirci un po’ tutti come a casa di Marianne. Il governo italiano ha da pochi giorni approvato un decreto legge che prevede non solo l’oscuramento dei siti che sostengono la guerra (santa o sociale? una mera questione di sfumature) contro questo mondo, ma anche la detenzione per chi andrà a combattere all’estero (come fanno nel presente alcuni islamisti o come facevano soprattutto in passato molti rivoluzionari?). Forse potremmo trasferirci nella civile e neutrale Svizzera, bizzarro paese dove la polizia preleva il Dna dei sovversivi direttamente dai manifesti affissi o dai volantini distribuiti.

Prevenire, prevenire, ancora prevenire.

*** Consenso ed obbedienza

Esiste una profonda differenza nella maniera in cui democrazia e totalitarismo, sinistra e destra, guardano alla questione di come mantenere il potere. La sinistra cerca il consenso, e per questo predilige le buone maniere. La destra esige obbedienza, e per questo ricorre alla forza. La prima è cordiale, ama ispirare fiducia, è ipocrita. La seconda è rozza, spesso odiosa, ma più sincera. Essendo sorelle siamesi, due facce dello stesso organismo, per conoscerne la vera natura la cosa migliore è rivolgersi a chi non ha peli sulla lingua. È sufficientemente noto che <em>non esistono amici sinceri, sinceri sono solo i nemici.</em> Ecco perché è inutile prestare troppa attenzione ai discorsi delle anime belle che vorrebbero salvare lo stato di diritto dal baratro delle sue eccezioni. Le loro diffuse lamentele, così prevedibili, le conosciamo ormai a memoria. Ma per capire dove vogliono andare a parare gli attuali padroni del mondo, meglio trattenere il respiro e porgere l’orecchio alla bocca dei loro mastini. Uno di questi è il giurista tedesco Günther Jakobs, il quale ha fatto inorridire i suoi colleghi umanitari sostenendo e motivando apertamente le ragioni per cui «i terroristi non hanno diritti». Secondo Jakobs la rottura del patto sociale, la trasgressione della legge, può far perdere all’individuo il suo status di cittadino. In passato una simile tesi era stata già sostenuta nei casi di alto tradimento (da Hobbes), o nei casi di minaccia costante alla sicurezza (da Kant). Il diritto penale procede sempre su due binari distinti, quello dialogante ed includente da una parte, e quello neutralizzante ed escludente dall’altra. Se chi infrange la legge viene ritenuto recuperabile, lo Stato lo considera un semplice <em>delinquente</em> a cui spettano comunque tutti i diritti di cittadino. Anche qualora avesse violato una norma, egli non nega la legge radicalmente. Ma al trasgressore della legge per convinzione, a colui che si pone <em>al di fuori</em> dell’ordine sociale minacciandolo costantemente, diventando così non un mero sporadico disobbediente ma un vero e proprio avversario dello Stato, non si può applicare il medesimo trattamento. Questo perché, a detta del giurista teutonico, costui non mostra una sufficiente «garanzia cognitiva», cioè la capacità e la disponibilità di riconoscere l’ordinamento istituzionale. Non è un delinquente da punire, <em>è</em> <em>un nemico da eliminare</em>. In questo senso non va considerato nemmeno una «persona», cioè un soggetto rispetto al quale sia ancora possibile un dialogo da parte dello Stato, e perciò contro di lui va mossa una <em>guerra</em>. Esattamente come contro un nemico (o contro torme di ratti morali, o di zecche).

Con impeccabile coerenza logica Jakobs precisa che, qualora un individuo non offra la garanzia di un possibile ravvedimento, «lo Stato non <em>deve</em> trattarlo come persona, giacché in caso contrario lederebbe il diritto alla sicurezza delle altre persone». Nei confronti degli individui che non riconoscano l’ordinamento giuridico, lo Stato deve applicare il diritto penale del nemico: un diritto che guarda al futuro (per neutralizzare pericoli) e non al passato (per riaffermare la validità di una norma).

Herr Jakobs parla chiaro e spiega perché stiamo assistendo, nell’ambito del diritto, ad un progressivo sganciamento tra fatto in sé giudicato e pena sanzionata. Dopo che alcuni partecipanti a scontri di strada contro la polizia nel 2001 sono stati condannati a oltre 10 anni di galera, non c’è da stupirsi se oggi corre il rischio di finire in prigione anche chi, nel chiuso della sua camera, comunica attraverso un computer connesso in rete il suo dissenso nei confronti dello Stato. Lungo questa china, diventa allora quasi una conseguenza inevitabile il fatto che nel 2014 un manifestante venga ammazzato da una granata. Non nel pericoloso deserto siriano, ma in una placida campagna francese.

Perché se lo Stato guarda al futuro, cosa vede? Crack economici, disoccupazione di massa, esaurimento delle risorse, conflitti bellici internazionali, guerre civili, catastrofi ambientali, esodi, sovraffollamento... Vede insomma un mondo sempre più piccolo, sempre più povero, trasudante disperazione, che si sta trasformando in una polveriera enorme, pregna di tensioni di ogni genere (sociali, etniche, religiose). Un mondo in cui non va tollerata l’accensione della minima scintilla, quale che sia. Se vuole mantenere l’ordine, se vuole proteggere la propria sicurezza, allo Stato non rimane che una strada: chiudere ogni spazio di movimento, sorvegliare ogni forma di libertà, schedare ogni singolo individuo. Anche se non è minacciato da una forza avversaria, lo Stato non può che farsi totalitario. Necessità resa facile da soddisfare dalla tecnologia moderna, che gli permette di non aver più bisogno di riempire le strade col rimbombo degli stivali. Milioni di persone marceranno al passo in punta di piedi, nel silenzio delle pantofole, lasciandogli mantenere le apparenze più democratiche. Anche perché lo Stato può sempre fare affidamento su quella soggezione interiore che impone agli individui di accettare di proprio gradimento, quasi con sollievo, ogni procedimento poliziesco (come avviene in qualche caso di stupro, dove intere comunità si sottopongono volontariamente al prelievo del Dna per evitare di destare sospetti). Se è vero che il diritto non determina i rapporti sociali ma li riflette, allora c’è da interrogarsi su cosa sia diventato l’essere umano, su cosa siamo diventati tutti noi. Ed iniziare a trarne le conseguenze, senza rifugiarsi nella tradizione o nella mitopoiesi.

*** Quali conseguenze

Alla fine dell’Ottocento vennero varate in Francia una serie di leggi volte a stroncare un movimento anarchico da cui erano appena usciti Ravachol e Auguste Vaillant, Emile Henry e Sante Caserio. Provvedimenti punitivi, talmente duri da passare alla storia come «leggi scellerate». Un termine facile da ricordare soprattutto perché facile da comprendere. Scellerate sono le leggi brutte, cattive, esagerate. Quelle che non vanno confuse con le leggi belle, buone, giustificate. Con le leggi normali, insomma. Si potrebbe quasi dire con le leggi giuste.

Eccoci qua, ci siamo. Se una volta tanto fossimo sinceri, magari da soli, davanti ad uno specchio, senza nessuno a cui rendere conto, potremmo ammettere che, per quanto critichiamo lo Stato, per quanto urliamo il nostro odio per la sua ferocia e la sua violenza, <em>noi non crediamo fino in fondo a queste nostre parole.</em> Noi siamo i primi a non credere alle nostre idee. Sì, in teoria, a grandi linee, in generale... ma poi, nella pratica, suvvia... spesso si tratta di esagerazioni!

Nella teoria, siamo bravi a sostenere come non esistano sostanziali differenze fra totalitarismo e democrazia, che sono due forme alterne di potere che un regime può assumere a seconda delle circostanze. Siamo abili nell’osservare come la riduzione dell’essere umano a mero numero si concretizzi sia nel tatuaggio sul braccio dei prigionieri del nazismo sia nei codici sulle pratiche dei prigionieri della burocrazia. Siamo capaci di discettare sulla similitudine e continuità fra i vecchi posti di blocco e le moderne telecamere di videosorveglianza. Siamo pronti a notare come la biometria o le banche-dati del Dna avrebbero fatto la gioia delle SS. Ma, nella pratica, quanto ci crediamo e siamo conseguenti? Vediamo i nostri frigoriferi pieni (ancora per quanto?), guardiamo in poltrona la partita di calcio, indossiamo i nostri abiti privi di macchie di sangue, e mentre ci accingiamo ad andare al bar ci diciamo: no, non è la stessa cosa. Così, se vediamo uno Stato inasprire la sua legislazione per proteggersi da chi non gli obbedisce, tutta la nostra consapevolezza teorica radicale scompare e si ricade dritti nell’indignazione pratica democratica. Allora si va a scavare dentro il diritto, a quel diritto di cui fino al giorno prima si metteva in luce la pura menzogna, alla ricerca di fantomatiche verità tradite o sospese. Si denunciano stati di eccezione al fine di pretendere il ripristino di stati di diritto.

Pensiamo, su un altro livello, al gran parlare che da qualche tempo si fa di un rapporto della NATO risalente al 2003, rapporto che prenderebbe in considerazione l’impiego dell’esercito in operazioni urbane entro il 2020. C’è chi lo ha peritosamente letto, analizzato, studiato, vivisezionato, per poi proclamarne gli strabilianti risultati: l’esercito verrà usato anche nelle nostre strade! Non solo nel passato e nel presente, ma pure nel futuro. E la novità, in cosa consisterebbe? Non certo nel suo utilizzo in situazioni con possibili sbocchi insurrezionali. Tralasciando i carri armati inglesi a Belfast, pur sempre truppe di occupazione, che dire dei cingolati a Bologna nel 1977 (o nella assai più tranquilla Voghera nel 1983, nel corso di una manifestazione repressa anche dalle teste di cuoio)? E i militari che pattugliano da anni, mitra spianato, i luoghi “sensibili” di alcune metropoli?

Può darsi che si tratti solo di una questione di approccio informativo. Può darsi che si dia per scontato che il modo migliore per comunicare con gli altri — questi altri che sovversivi non sono — sia quello di condividerne il linguaggio legalitario, lo stupore umanitario, le rivendicazioni riformiste, le pastoie democratiche. Come se, allo scopo di trascinare le masse, si cercasse prima di arpionarle, conficcarsi al loro interno per poterle agganciare saldamente. Ma, così facendo, non si fa che cullarne le illusioni, ribadire le loro allucinazioni, confermare i loro fantasmi.

Per i moderni sonnambuli dormienti, è tempo di bruschi risvegli, non di suadenti bisbigli o di guide illuminate all’interno dei loro luoghi comuni. Se è rimasto ancora qualcosa da comunicare, se non si vuole tacere per non apportare il proprio stridio al frastuono contemporaneo, allora non rimane che urlare a squarciagola la nostra scomoda verità.

Che non esiste nessuna deriva totalitaria, ma solo una doppia marcia di velocità in quella che lo Stato — qualsiasi Stato — considera la retta via dell’esercizio del potere.

*** I punti sensibili

La scorsa estate Anthony Glees, docente di sicurezza ed intelligence alla Buckingham University, ha rilasciato una dichiarazione: «Abbiamo cercato di rendere l’estremismo qualcosa per cui non vale la pena correre rischi, ma nonostante tutto ciò evidentemente continuiamo ancora a generare jihadisti. Sono giunto alla conclusione che siamo stati troppo sensibili alla lobby delle libertà civili — gente che afferma che siamo una società multiculturale e che due insiemi di valori fondamentali possano stare felicemente seduti fianco a fianco nel Regno Unito. Abbiamo permesso che delle persone andassero in giro per il paese a predicare l’estremismo e la violenza con la scusa della religione e della libertà di parola».

Il poco simpatico docente inglese non ha tutti i torti. È vero, nonostante la minaccia della repressione, questo mondo miserabile continua a produrre insoddisfatti, arrabbiati, ribelli, pronti ad insorgere per le ragioni più svariate. È vero, due diversi insiemi di valori fondamentali (come quelli legati all’autorità o alla libertà) non possono stare felicemente fianco a fianco. È vero, non si può più permettere che ci sia chi va in giro a predicare la violenza (del capitalismo) con la scusa della libertà di parola. Bisognerà cominciare a porvi rimedio.

Nel linguaggio burocratico udibile nei palazzi di vetro di Bruxelles, dietro alla sigla EPCIP si nasconde il «programma europeo di protezione delle infrastrutture critiche». Attivo già da anni, «Il piano d’azione EPCIP è organizzato intorno a tre assi di intervento: il primo verte sugli aspetti strategici dell’EPCIP e sull’elaborazione di misure applicabili orizzontalmente a tutti i lavori in ambito PIC (Protezione Infrastrutture Critiche); il secondo riguarda la protezione delle infrastrutture critiche e mira a ridurne la vulnerabilità; il terzo concerne l’ambito nazionale e si propone di aiutare gli Stati membri a proteggere le loro ICN (Infrastrutture Critiche Nazionali). Il piano d’azione implica un processo continuo e deve essere riesaminato periodicamente».

Il motivo di questa consultazione permanente fra governi europei è presto detto: «Tutte le parti in causa devono scambiare le informazioni relative alla protezione delle infrastrutture critiche, segnatamente le informazioni riguardanti questioni come la sicurezza delle infrastrutture critiche e dei sistemi protetti, gli studi sulle interdipendenze, i punti vulnerabili, la valutazione delle minacce e dei rischi. Al contempo, occorre fare in modo che le informazioni esclusive, sensibili o di carattere personale scambiate non siano rese pubbliche e che chiunque tratti informazioni riservate o sensibili sia soggetto a un’appropriata verifica di sicurezza da parte dello Stato membro di cui è cittadino». Questo perché «Considerato il livello di interconnessione ed interdipendenza delle economie moderne, la perturbazione o la distruzione di un’infrastruttura critica europea potrebbe comportare delle conseguenze per i paesi esterni all’Unione europea e viceversa. Pertanto, è indispensabile consolidare la cooperazione internazionale nel settore mediante protocolli d’intesa settoriali».

Resta quindi da capire cosa si intenda per «infrastrutture critiche». Esse sono: «le risorse materiali, i servizi di tecnologia dell’informazione, le reti e i beni infrastrutturali che, se danneggiati o distrutti, causerebbero gravi ripercussioni sulla salute, la sicurezza e il benessere economico dei cittadini o sul funzionamento dei governi». Stabilita l’importanza di proteggere simili infrastrutture, i burocrati europei si sono subito messi al lavoro ed hanno diffuso una prima direttiva che, nella sua prima fase, «si riferisce esplicitamente ai settori dell’energia e dei trasporti».

<em>Energia e trasporti</em>: ecco i punti sensibili del dominio. Perché sono questi a consentire tecnicamente la riproduzione dell’esistente, fra cui spicca la produzione, la circolazione ed il consumo di dati e di merci, nonché il funzionamento di ogni genere di macchina. Senza energia e senza trasporti, la vita quotidiana così come la conosciamo — quella al servizio dello Stato — si incepperebbe, rallenterebbe, si fermerebbe. Un’interruzione di quei flussi, soprattutto se prolungata ed estesa, potrebbe provocare un effetto domino dagli esiti imprevedibili, come sostengono quei rapporti.

Nel momento in cui nulla fosse più come prima, tutto diverrebbe possibile. Che terribile prospettiva!

<right>
<em>finimondo.org</em>

(febbraio 2015)
</right>

          * * * * * 

*** ALCUNE NOVITÀ IN SPAGNA

<em>Con la riforma della «Legge Mordaza»: </em>

<strong>Infrazioni molto gravi (da 30 000 a 600 000 euro):</strong>

 -  Manifestazioni non-autorizzate contro il funzionamento di infrastrutture critiche (centrali nucleari, aeroporti) – Fabbricazione o detenzione illegale di armi e uso non-conforme di esplosivi catalogati (es. fuochi d’artificio)

<strong>Infrazioni gravi (da 1000 a 30 000 euro): </strong>

 -  Disturbo dell’ordine pubblico nel corso di eventi pubblici, sportivi, culturali, religiosi o altri raggruppamenti

 -  Manifestazioni davanti a sedi del Congresso Deputati, del Senato e dei parlamenti delle comunità autonome

 -  Scontri e incendi dolosi sulla pubblica via (di cassonetti, ecc.)

 -  Intralcio ai diversi poteri pubblici, ad esempio nel corso di espulsioni da parte di ufficiali giudiziari

 -  Non ottemperare all’ordine di dispersione nei concentramenti su richiesta della polizia

 -  Non collaborare con le forze dell’ordine per prevenire un reato

 -  Consumo ed uso di droghe

 -  Uso o pubblicazione non autorizzata di immagini o di video delle forze dell’ordine o delle autorità

<strong>Infrazioni leggere (da 100 a 1000 euro):</strong>

 -  Manifestazioni o presidi non-autorizzati

 -  Esibire oggetti pericolosi con l’intenzione d’intimidire

 -  Insultare o minacciare agenti di polizia, anche nei cortei – Occupazione di spazi comuni, pubblici o privati

 -  Graffiti, vandalismo contro l’arredo urbano, etc.

 -  Scalare edifici o monumenti

*** ALCUNE NOVITÀ IN GRECIA

<em>Ristrutturazione delle carceri, ora divise in tre categorie: A, B e C. </em>

La prigione di tipo C è riservata ai condannati per terrorismo, organizzazione criminale e ai detenuti ribelli e portatori di tumulti.

I condannati per terrorismo a più di 12 anni, vanno direttamente nel Tipo C. In altri casi, sarà un procuratore a decidere circa il regime da applicare in funzione della pericolosità del detenuto, etc.

La durata: 2 anni di Tipo C per i più recalcitranti; 4 anni per i condannati per terrorismo e organizzazione criminale. Possibilità di proroga.

Il regime di tipo C: ore di visita limitate (alcune ore al mese), nessuna attività, né permessi, un’ora d’aria al giorno, censura della posta.

Aumento dei poteri dei secondini all’interno e dei poliziotti che sorvegliano l’esterno delle carceri. Per il tipo C, la sorveglianza verrà affidata ad unità speciali di polizia.

Introduzione del braccialetto elettronico per chi usufruisce della condizionale e di un permesso, introduzione dei domiciliari.

La nuova legge prevede il prelievo del DNA nel caso di reati che prevedono una condanna dai 3 mesi in su. (In caso d’innocenza il DNA sarebbe eliminato, vorrebbero farci credere).

Il tipo C, è una prigione all’interno della prigione. La legge interviene adesso in un particolare contesto: isolamento e annientamento delle esperienze di «guerriglia urbana», minaccia del movimento rivoluzionario nel suo complesso.

Lo Stato ha svuotato una sezione della prigione d’alta sicurezza di Domokos, per installarvi la prima sezione C. Dalla fine del 2014 sono già avvenuti i primi trasferimenti.

Più in generale, le «novità» repressive in Grecia, sono:

Perquisizioni di massa (come dopo il non rientro da un permesso di Xiros, ex “17 Novembre”, circa 100 perquisizioni in tutta la Grecia). Accusa di terrorismo e appartenenza alle “CCF” per qualsiasi attacco incendiario o esplosivo. Uso del DNA come prova assoluta.

*** ULTIME NEL MONDO, MA IN CONTINUA EVOLUZIONE

**** ITALIA

La nuova legge prevede modifiche al codice penale introducendo una pena da tre a sei anni di reclusione per chi va a combattere il jihad nei teatri di guerra o sostiene i combattenti organizzando, finanziando e facendo propaganda, anche via web. Fino a 10 anni di carcere per i lupi solitari, che si autoaddestrano all’uso di armi. Sarà inoltre istituita una black list dei siti internet che sostengono il terrorismo e sarà possibile oscurarli su disposizione dell’autorità giudiziaria. Viene istituito un coordinamento centrale presso la Procura nazionale antimafia e rafforzata l’intelligence, le operazioni sotto copertura e le garanzie funzionali per gli infiltrati.

**** CANADA 

Col progetto di legge C-51, vengono aumentati i poteri delle varie agenzie nazionali di sicurezza. Dalla semplice raccolta di informazioni (da passare poi alla Gendarmeria reale del Canada), ora potranno intervenire direttamente. Viene istituito il reato di «istigazione» che prevede una pena massima di cinque anni di prigione per chiunque inciti «a condurre attacchi contro il Canada». Sarà più facile procedere con arresti. La detenzione preventiva passa da tre a sette giorni. Il progetto di legge mira anche ad allargare le misure che vietano gli spostamenti per chi è sospettato di costituire una minaccia.

**** BELGIO 

Anche il parlamento belga a metà febbraio dovrà esaminare le nuove proposte di legge anti-terrorismo. Si tratta di 12 misure che prevedono fra l’altro l’estensione delle «infrazioni terroriste» e dei mezzi per contrastarle, il ritiro della nazionalità come sanzione facoltativa, il ritiro temporaneo di carta d’identità e passaporto, il blocco dei conti correnti, la differenziazione nelle carceri, il ricorso all’esercito per missioni specifiche di sorveglianza.

**** SPAGNA 

A inizio febbraio le due principali forze politiche — il Partito Popolare al potere, e quello Socialista all’opposizione — hanno presentato una nuova proposta di legge che prevede l’introduzione della pena dell’ergastolo per chi è giudicato colpevole di aver commesso un attentato che ha provocato la morte di qualcuno. In Spagna la pena massima prevista fino ad oggi era di 40 anni di detenzione, essendo stato abolito l’ergastolo nel 1975 dopo la fine del regime del generale Franco. Norme anche per ostacolare la diffusione di messaggi via internet ed i viaggi all’estero verso zone di guerra. Le risorse di giustizia e polizia verranno rafforzate per contrastare ogni tipo di «terroristi», compresi i «lupi solitari» al di fuori da ogni organizzazione.

**** INGHILTERRA 

Il primo ministro Cameron sta per rafforzare l’arsenale legislativo contro la minaccia sovversiva. Fra le misure in discussione c’è il ritiro della cittadinanza per i sospetti jihadisti in rientro da zone di guerra, con la conseguente messa al bando dal territorio britannico per un periodo fino a due anni. Ciò li renderebbe apolidi, impossibilitati ad essere riconosciuti legalmente. Verrà proibito alle assicurazioni di pagare qualsivoglia riscatto ad islamisti. Inoltre verrà assegnato alle istituzioni educative, dagli asili fino alle università, il compito di identificare e segnalare le possibili teste calde. Previsto anche un giro di vite su internet.

**** CAMERUN 

Intanto in questo paese africano a metà gennaio due leader sindacali sono stati arrestati dopo aver firmato un preavviso di sciopero, con l’accusa di «apologia di crimine, sedizione e attività terrorista». Si tratterebbe di una «detenzione amministrativa» della durata di 15 giorni, ma rinnovabile una volta.

**** NUOVA ZELANDA 

Nuove leggi varate dal parlamento consentono una sorveglianza video 24 ore su 24 senza mandato e il ritiro del passaporto per tre anni per le persone sospettate di essere coinvolte nel terrorismo.




