#title Riapertura delle scuole per che fare?
#author Redazione di Catania
#SORTauthors Redazione di Catania
#SORTtopics studenti, scuola, università, lotte intermedie, lotte sociali, Anarchismo (rivista)
#date Settembre 1986
#source Anarchismo n.53-54 settembre 1986, consultato su [[https://roundrobin.info/2018/09/riapertura-delle-scuole-per-che-fare/][roundrobin.info]]
#lang it
#pubdate 2022-01-10T23:31:30

Riprende il rito annuale di riapertura delle scuole. Possiamo pronosticare, molto facilmente, che riemergeranno i soliti problemi: l’agibilità delle scuole, le disponibilità degli insegnanti, le tasse, la legge finanziaria, i trasporti pubblici, la disoccupazione, ecc. E, altrettanto certamente, si può ipotizzare che riemergerà un movimento degli studenti con proposte più o meno identiche a quelle dell’anno scorso. Rivendicative e riformiste. Proposte scontate e movimento altrettanto ovvio.

**Difficile**

Può forse l’insegnamento del’anno scorso servire a qualcosa? Può il fallimento delle grandi mobilitazioni su temi generici e biecamente parziali, servire a fare aprire gli occhi agli studenti che, per la maggior parte, sono gli stessi dell’anno scorso? Pensiamo che la cosa sarà molto difficile. Comunque possiamo sempre insistere sul problema della funzione della scuola e portare quanti più giovani possibili ad una coscienza di classe nei riguardi di questa istituzione che serve, appunto, come strumento di qualificazione ridotta allo scopo di consentire l’accettazione del dominio, una migliore flessibilità della mano d’opera a una sua adattabilità alle mutate condizioni del mercato del lavoro.

**Nessuna Prospettiva**

La scuola di oggi non produce più qualificazione (né tecnica, né umanistica), ma si è adattata alla realtà del sistema produttivo attuale. Essa produce pace sociale, convincendo i giovani della possibilità di ottenere studiando, un diverso status sociale, rendendolo più malleabile, ma, nello stesso tempo, fornendo pochi strumenti effettivi di conoscenza e di cultura e, per giunta, strumenti dequalificati, confezionati in formato ridotto ad uso delle attuali generazioni e della loro pseudo cultura massificata. Mai come oggi la scuola è stata tanto trasparente. Puleggia del potere, veicola i contenuti ridotti che consentono di tenere a bada una larga parte della massa. Il vecchio analfabetismo, su cui si basava il potere del passato, è stato ammodernato e mantenuto nella scuola di oggi che produce analfabeti di nuovo tipo, i quali sanno leggere e scrivere, ma solo quelle poche cose che il potere ha previsto che potessero leggere e scrivere. Mettete uno di questi giovani davanti ad un testo impegnativo o davanti la necessità di scrivere qualcosa di più complesso dei risultati delle partite di calcio e potete constatare le enormi difficoltà cui andrà incontro. Tutto ciò (con le dovute eccezioni, evidentemente) non è accidentale. Accertata la funzione repressiva e mistificante della scuola bisognerà decidersi su cosa fare. Ci pare esistano due strade. Distruggerla o utilizzarla.

**Distruzione**

Distruggerla è la soluzione migliore. Più efficace e sbrigativa. In questo caso il sabotaggio, l’attacco diretto, la violenza rivoluzionaria contro le cose e le persone che fanno parte della scuola come padroni e gestori, sono le cose più efficaci ed immediate da fare. Utilizzarla è la cosa più difficile

**Utilizzazione**

Occorre procedere con cautela per evitare di essere utilizzati invece di strappare via qualcosa al potere. In fondo, però, la scuola è un laboratorio dove di possono ancora trovare pochi strumenti culturali di carattere elementare. Questi strumenti possono essere strappati alla scuola con metodo e costanza, ed anche con una lotta che può utilizzare, ad esempio, lo strumento dell’autogestione. In questo senso, il movimento delle occupazioni delle scuole può riscoprire un nuovo modo di lottare che forse potrà essere recuperato difficilmente dal potere.

<right>
Redazione di Catania
</right>
